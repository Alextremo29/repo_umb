/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AuditLogService .java
 * Created on: Tue Jul 11 16:47:06 COT 2017
 * Project: sarpa
 * Objective: To audit actions over entityes
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.audit;

import com.gml.sarpa .api.entity.audit.AuditLog;
import java.util.List;

/**
 * To audit actions over entityes
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AuditLogService{

    /**
     * List all entities
     * @return
     */
    List<AuditLog> findAll();

    /**
     * Save entity
     * @param auditLog
     */
    void save(AuditLog auditLog);

    /**
     * Update entity
     * @param auditLog
     */
    void update(AuditLog auditLog);

    /**
     * Delete entity
     *@param auditLog
     */
    void remove(AuditLog auditLog);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    AuditLog getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<AuditLog> getAllByFilter(String filter);

    /**
     * Create audit for object insert action
     * @param object
     * @return
     */
    void auditInsert(String className,Long objectId,
            Integer version,String data,String userName);
    
    /**
     * Create audit for object insert action
     * @param object
     * @return
     */
    void auditUpdate(String className,Long objectId,
            Integer version,String data,String property, String value,String userName);

    /**
     * Create audit for object delete action
     * @param object
     * @return
     */
    void auditDelete(String className,Long objectId,
            Integer version,String data,String userName);

    
}
