/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserService.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: Interface to define user services
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.auth;

import com.gml.sarpa.api.entity.auth.User;
import java.util.List;



/**
 * Interface to define user services
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface UserService {

    

    /**
     * Save an user
     *
     * @param user
     */
    void save(User user,String userName);

    
    /**
     * Find a client user by username 
     *
     * @param dni
     */
    User findByUsername(String username);

    /**
     * Find a client user by username 
     *
     * @param dni
     */
    User findByUsernameAndPassword(User user);
    
    /**
     * Update entity
     * @param user
     */
    void update(User user,String userName);

    /**
     * Delete entity
     *@param user
     */
    void remove(User user,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    User getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<User> getAllByFilter(String filter);
    
    /**
     * List all entities
     * @return
     */
    List<User> findAll();
   
}
