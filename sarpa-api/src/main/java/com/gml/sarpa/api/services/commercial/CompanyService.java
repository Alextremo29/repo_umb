/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CompanyService.java
 * Created on: Wed Jul 19 16:36:43 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.Company;
import java.util.List;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CompanyService{

    /**
     * List all entities
     * @return
     */
    List<Company> findAll();

    /**
     * Save entity
     * @param company
     */
    void save(Company company,String userName);

    /**
     * Update entity
     * @param company
     */
    void update(Company company,String userName);

    /**
     * Delete entity
     *@param company
     */
    void remove(Company company,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Company getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Company> getAllByFilter(String filter);

    
}
