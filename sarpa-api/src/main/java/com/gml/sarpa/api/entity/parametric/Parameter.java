/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Session.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: DA - Débito automático.
 * Objective: To represents a system parameter.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.parametric;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entity for represents a system parameter
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Table(name = "PARAMETER", schema = "parametric",uniqueConstraints = {
    @UniqueConstraint(columnNames = {"KEY"},name = Parameter.UC_PARAMETER_KEY)
})
@Entity
public class Parameter implements Serializable {

    public static final String UC_PARAMETER_KEY =
        "UC_PARAMETER_KEY";
    
    /**
     * ID.
     */
    @Id
    @Column(name = "ID")
    private Long id;
    
    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
     * Key
     */
    @Column(name = "KEY", nullable = false, unique = true)
    private String name;
    
    
    /**
     * Value.
     */
    @Column(name = "VALUE", nullable = false)
    private String value;
    
    /**
     * Description.
     */
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;
    
    /*-----------------------------------------------------------------------*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    /*-----------------------------------------------------------------------*/

    @Override
    public String toString() {
        return "Parameter{" + "id=" + id + ", name=" + name + ", value=" + value + '}';
    }

    
    
}
