/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneService .java
 * Created on: Thu Jun 29 15:09:23 COT 2017
 * Project: sarpa
 * Objective: To define Airplans
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.operation;

import com.gml.sarpa.api.entity.operation.Airplane;
import java.util.List;

/**
 * To define Airplans
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AirplaneService{

    /**
     * List all entities
     * @return
     */
    List<Airplane> findAll();

    /**
     * Save entity
     * @param airplane
     */
    void save(Airplane airplane,String userName);

    /**
     * Update entity
     * @param airplane
     */
    void update(Airplane airplane,String userName);

    /**
     * Delete entity
     *@param airplane
     */
    void remove(Airplane airplane,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Airplane getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Airplane> getAllByFilter(String filter);

    
}
