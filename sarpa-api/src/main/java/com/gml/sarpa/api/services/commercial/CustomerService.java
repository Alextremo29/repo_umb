/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerService.java
 * Created on: Tue Jul 25 20:03:42 COT 2017
 * Project: sarpa
 * Objective: To manage curstomers
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.Customer;
import java.util.List;

/**
 * To manage curstomers
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CustomerService{

    /**
     * List all entities
     * @return
     */
    List<Customer> findAll();

    /**
     * Save entity
     * @param customer
     */
    void save(Customer customer,String userName);

    /**
     * Update entity
     * @param customer
     */
    void update(Customer customer,String userName);

    /**
     * Delete entity
     *@param customer
     */
    void remove(Customer customer,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Customer getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Customer> getAllByFilter(String filter);

    
}
