/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Airport .java
 * Created on: Wed Jun 28 07:47:20 COT 2017
 * Project: sarpa
 * Objective: To define airports
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.operation;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * To define airports
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "airport", schema = "operation", uniqueConstraints = {
})
public class Airport implements Serializable{

      
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
    * code
    */
    @Transient
    private String code;
    
    /**
    * name
    */
    @Column(name = "name", nullable = false)
    private String name;

    /**
    * city
    */
    @Column(name = "city", nullable = false)
    private String city;

    /**
    * country
    */
    @Column(name = "country", nullable = false)
    private String country;

    /**
    * iataCode
    */
    @Column(name = "iata_code", nullable = false)
    private String iataCode;

    /**
    * icaoCode
    */
    @Column(name = "icao_code", nullable = false)
    private String icaoCode;

    /**
    * latitude
    */
    @Column(name = "latitude", nullable = false)
    private Double latitude;

    /**
    * longitude
    */
    @Column(name = "longitude", nullable = false)
    private Double longitude;

    /**
    * altitude
    */
    @Column(name = "altitude", nullable = false)
    private Long altitude;

    /**
    * timeZone
    */
    @Column(name = "time_zone", nullable = false)
    private Integer timeZone;

    /**
    * dst
    */
    @Column(name = "dst", nullable = false)
    private String dst;

    /**
    * tzTimezone
    */
    @Column(name = "tz_timezone", nullable = false)
    private String tzTimezone;

    /**
    * flgBase
    */
    @Column(name = "flg_base", nullable = false)
    private Boolean flgBase;

    /**
    * flgDestination
    */
    @Column(name = "flg_destination", nullable = false)
    private Boolean flgDestination;

    /**
    * owner
    */
    @Column(name = "owner", nullable = false)
    private String owner;

    /**
    * operator
    */
    @Column(name = "operator", nullable = false)
    private String operator;

    /**
    * trackLength
    */
    @Column(name = "track_length", nullable = false)
    private Long trackLength;

    /**
    * trackWidth
    */
    @Column(name = "track_width", nullable = false)
    private Long trackWidth;

    /**
    * operationSchedule
    */
    @Column(name = "operation_schedule", nullable = false)
    private String operationSchedule;

    /**
    * flgNightOperation
    */
    @Column(name = "flg_night_operation", nullable = false)
    private Boolean flgNightOperation;

    /**
    * flgRequiresPermission
    */
    @Column(name = "flg_requires_permission", nullable = false)
    private Boolean flgRequiresPermission;

    /**
    * permissionDetail
    */
    @Column(name = "permission_detail", nullable = false)
    private String permissionDetail;


    /*
     * -----------------------------------------------------------------------
     */
    
    public Airport() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
    
    public String getName() {
        return name;
    }

    public String getCode() {
        int nameLng =0;
        if (this.name != null){
            if (this.name.length()< 15){
                nameLng = this.name.length();
            }else{
                nameLng = 15;
            }
        }
        return this.city+"-"+this.icaoCode+"-"+this.getName().substring(0,nameLng);
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getAltitude() {
        return altitude;
    }

    public void setAltitude(Long altitude) {
        this.altitude = altitude;
    }

    public Integer getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(Integer timeZone) {
        this.timeZone = timeZone;
    }

    public String getDst() {
        return dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public String getTzTimezone() {
        return tzTimezone;
    }

    public void setTzTimezone(String tzTimezone) {
        this.tzTimezone = tzTimezone;
    }

    public Boolean getFlgBase() {
        return flgBase;
    }

    public void setFlgBase(Boolean flgBase) {
        this.flgBase = flgBase;
    }

    public Boolean getFlgDestination() {
        return flgDestination;
    }

    public void setFlgDestination(Boolean flgDestination) {
        this.flgDestination = flgDestination;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Long getTrackLength() {
        return trackLength;
    }

    public void setTrackLength(Long trackLength) {
        this.trackLength = trackLength;
    }

    public Long getTrackWidth() {
        return trackWidth;
    }

    public void setTrackWidth(Long trackWidth) {
        this.trackWidth = trackWidth;
    }

    public String getOperationSchedule() {
        return operationSchedule;
    }

    public void setOperationSchedule(String operationSchedule) {
        this.operationSchedule = operationSchedule;
    }

    public Boolean getFlgNightOperation() {
        return flgNightOperation;
    }

    public void setFlgNightOperation(Boolean flgNightOperation) {
        this.flgNightOperation = flgNightOperation;
    }

    public Boolean getFlgRequiresPermission() {
        return flgRequiresPermission;
    }

    public void setFlgRequiresPermission(Boolean flgRequiresPermission) {
        this.flgRequiresPermission = flgRequiresPermission;
    }

    public String getPermissionDetail() {
        return permissionDetail;
    }

    public void setPermissionDetail(String permissionDetail) {
        this.permissionDetail = permissionDetail;
    }

   
    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Airport{" + "name=" + name + ", city=" + city + ", country=" + 
                country + ", iataCode=" + iataCode + ", icaoCode=" + icaoCode + 
                ", latitude=" + latitude + ", longitude=" + longitude + 
                ", altitude=" + altitude + ", timeZone=" + timeZone + ", dst=" + 
                dst + ", tzTimezone=" + tzTimezone + ", flgBase=" + flgBase +
                ", flgDestination=" + flgDestination + ", owner=" + owner + 
                ", operator=" + operator + ", trackLength=" + trackLength + 
                ", trackWidth=" + trackWidth + ", operationSchedule=" + 
                operationSchedule + ", flgNightOperation=" + flgNightOperation + 
                ", flgRequiresPermission=" + flgRequiresPermission + 
                ", permissionDetail=" + permissionDetail + '}';
    }    
}
