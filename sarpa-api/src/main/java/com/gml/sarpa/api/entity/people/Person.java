/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Person .java
 * Created on: Fri Jun 30 22:34:42 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.people;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "person", schema = "people", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"document_number"},name = Person.UC_PERSON_DOCUMENT_NUMBER),
})
public class Person implements Serializable{

    public static final String UC_PERSON_DOCUMENT_NUMBER =
        "UC_PERSON_DOCUMENT_NUMBER";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;
    
    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
    * firstName
    */
    @Column(name = "first_name", nullable = false)
    private String firstName;

    /**
    * lastName
    */
    @Column(name = "last_name", nullable = false)
    private String lastName;

    /**
    * documentNumber
    */
    @Column(name = "document_number", nullable = false)
    private String documentNumber;

    /**
    * documentType
    */
    @Column(name = "document_type", nullable = true)
    private String documentType;
    
    /**
    * completeName
    */
    @Transient
    private String completeName;
    


    /*
     * -----------------------------------------------------------------------
     */
    
    public Person() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    
    public String getCompleteName() {
        return this.firstName+" "+lastName;
    }


    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Person{" + "firstName=" + firstName + ", lastName=" + lastName + ", documentNumber=" + documentNumber + ", documentType=" + documentType + ", completeName=" + completeName + '}';
    }

   
    
    
}
