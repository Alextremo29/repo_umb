/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BusinessUnitService .java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import java.util.List;

/**
 * To define business units
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface BusinessUnitService{

    /**
     * List all entities
     * @return
     */
    List<BusinessUnit> findAll();

    /**
     * Save entity
     * @param businessUnit
     */
    void save(BusinessUnit businessUnit,String userName);

    /**
     * Update entity
     * @param businessUnit
     */
    void update(BusinessUnit businessUnit,String userName);

    /**
     * Delete entity
     *@param businessUnit
     */
    void remove(BusinessUnit businessUnit,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    BusinessUnit getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<BusinessUnit> getAllByFilter(String filter);

    
}
