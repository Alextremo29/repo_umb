/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneType .java
 * Created on: Thu Jun 29 11:47:51 COT 2017
 * Project: sarpa
 * Objective: To define Airplane Types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.operation;

import com.gml.sarpa.api.entity.parametric.Currency;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To define Airplane Types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "airplane_type", schema = "operation", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"name"},name = AirplaneType.UC_airplane_type_name),
})
public class AirplaneType implements Serializable{

    public static final String UC_airplane_type_name =
        "UC_airplane_type_name";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
    * name
    */
    @Column(name = "name", nullable = false)
    private String name;

    /**
    * description
    */
    @Column(name = "description", nullable = false)
    private String description;

    /**
    * detail
    */
    @Column(name = "detail", nullable = false)
    private String detail;

    /**
    * autonomy
    */
    @Column(name = "autonomy", nullable = false)
    private Long autonomy;

    /**
    * maxDistance
    */
    @Column(name = "max_distance", nullable = false)
    private Long maxDistance;

    
    /**
    * businessUnit
    */
    @JoinColumn(name = "currency_id", nullable = true)
    @ManyToOne
    private Currency currency;

    /*
     * -----------------------------------------------------------------------
     */
    
    public AirplaneType() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getAutonomy() {
        return autonomy;
    }

    public void setAutonomy(Long autonomy) {
        this.autonomy = autonomy;
    }

    public Long getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(Long maxDistance) {
        this.maxDistance = maxDistance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "AirplaneType{" + "name=" + name + ", description=" + description 
                + ", detail=" + detail + ", autonomy=" + autonomy + ", maxDistance=" 
                + maxDistance  + '}';
    } 
}
