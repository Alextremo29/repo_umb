/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactService .java
 * Created on: Fri Jun 30 22:30:20 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.people;

import com.gml.sarpa.api.entity.people.Contact;
import java.util.List;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContactService{

    /**
     * List all entities
     * @return
     */
    List<Contact> findAll();

    /**
     * Save entity
     * @param contact
     */
    void save(Contact contact,String userName);

    /**
     * Update entity
     * @param contact
     */
    void update(Contact contact,String userName);

    /**
     * Delete entity
     *@param contact
     */
    void remove(Contact contact,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Contact getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Contact> getAllByFilter(String filter);
    
    /**
     * Search entity by param
     * @param value
     * @param fillValue
     * @return 
     */
    List<Contact> getContactsParams(String value, Long fillValue);

    
}
