/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PersonService .java
 * Created on: Fri Jun 30 22:34:42 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.people;

import com.gml.sarpa.api.entity.people.Person;
import java.util.List;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface PersonService{

    /**
     * List all entities
     * @return
     */
    List<Person> findAll();

    /**
     * Save entity
     * @param person
     */
    void save(Person person,String userName);

    /**
     * Update entity
     * @param person
     */
    void update(Person person,String userName);

    /**
     * Delete entity
     *@param person
     */
    void remove(Person person,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Person getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Person> getAllByFilter(String filter);

    
}
