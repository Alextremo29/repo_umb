/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TypeOfCompanyRelationshipService .java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.TypeOfCompanyRelationship;
import java.util.List;

/**
 * To define type of company relationship
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */

public interface TypeOfCompanyRelationshipService{

    /**
     * List all entities
     * @return
     */
    List<TypeOfCompanyRelationship> findAll();

    /**
     * Save entity
     * @param businessUnit
     */
    void save(TypeOfCompanyRelationship businessUnit,String userName);

    /**
     * Update entity
     * @param businessUnit
     */
    void update(TypeOfCompanyRelationship businessUnit,String userName);

    /**
     * Delete entity
     *@param businessUnit
     */
    void remove(TypeOfCompanyRelationship businessUnit,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    TypeOfCompanyRelationship getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<TypeOfCompanyRelationship> getAllByFilter(String filter);

    
}
