/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadTypeService .java
 * Created on: Fri Jun 30 23:16:25 COT 2017
 * Project: sarpa
 * Objective: To define load types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.operation;

import com.gml.sarpa.api.entity.operation.LoadType;
import java.util.List;

/**
 * To define load types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface LoadTypeService{

    /**
     * List all entities
     * @return
     */
    List<LoadType> findAll();

    /**
     * Save entity
     * @param loadType
     */
    void save(LoadType loadType,String userName);

    /**
     * Update entity
     * @param loadType
     */
    void update(LoadType loadType,String userName);

    /**
     * Delete entity
     *@param loadType
     */
    void remove(LoadType loadType,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    LoadType getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<LoadType> getAllByFilter(String filter);

    
}
