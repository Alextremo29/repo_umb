/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerRequestService.java
 * Created on: Wed Jul 26 17:22:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients requests
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.CustomerRequest;
import java.util.List;

/**
 * To manage clients requests
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CustomerRequestService{

    /**
     * List all entities
     * @return
     */
    List<CustomerRequest> findAll();

    /**
     * Save entity
     * @param customerRequest
     */
    void save(CustomerRequest customerRequest,String userName);

    /**
     * Update entity
     * @param customerRequest
     */
    void update(CustomerRequest customerRequest,String userName);

    /**
     * Delete entity
     *@param customerRequest
     */
    void remove(CustomerRequest customerRequest,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    CustomerRequest getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<CustomerRequest> getAllByFilter(String filter);

    
}
