/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: EnumOptions.java
 * Created on: 2017/09/13, 10:54:20 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.enums;

/**
 *
 * @author <a href="mailto:josesj@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
public enum EnumOptions {
    YES(0,"Si"), 
	NO(1,"No"); 
	
    private final Integer id;
    
	private final String name;

	private EnumOptions(Integer id, String name) {
        this.id = id;
		this.name = name;
	}
    /**
     * 
     * @return the id 
     */
    public int getId() {
        return id;
    }

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
}
