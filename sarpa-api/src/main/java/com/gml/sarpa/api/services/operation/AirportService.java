/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportService .java
 * Created on: Wed Jun 28 07:47:20 COT 2017
 * Project: sarpa
 * Objective: To define airports
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.operation;

import com.gml.sarpa.api.entity.operation.Airport;
import java.util.List;

/**
 * To define airports
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AirportService{

    /**
     * List all entities
     * @return
     */
    List<Airport> findAll();

    /**
     * Save entity
     * @param entityInstance
     */
    void save(Airport entityInstance,String userName);

    /**
     * Update entity
     * @param entityInstance
     */
    void update(Airport entityInstance,String userName);

    /**
     * Delete entity
     *@param entityInstance
     */
    void remove(Airport entityInstance,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Airport getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Airport> getAllByFilter(String filter);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Airport> findAllByFlgDestination(boolean flag);

    List<Airport> findAllByFlgBase(boolean flag);

    List<String> getCitiesInAirport(String city);
    
    List<Airport> getAirportsByCityOrNameOrIataCode(String valueAirport,
        Long fillValueAirport);

    
}
