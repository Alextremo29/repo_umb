/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactTypeService .java
 * Created on: Fri Jun 30 22:28:37 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.people;

import com.gml.sarpa.api.entity.parametric.ContactType;
import java.util.List;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContactTypeService{

    /**
     * List all entities
     * @return
     */
    List<ContactType> findAll();

    /**
     * Save entity
     * @param contactType
     */
    void save(ContactType contactType,String userName);

    /**
     * Update entity
     * @param contactType
     */
    void update(ContactType contactType,String userName);

    /**
     * Delete entity
     *@param contactType
     */
    void remove(ContactType contactType,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    ContactType getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<ContactType> getAllByFilter(String filter);

    
}
