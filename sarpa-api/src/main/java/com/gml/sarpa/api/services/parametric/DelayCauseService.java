/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DelayCauseService .java
 * Created on: Mon Jul 17 06:41:33 COT 2017
 * Project: sarpa
 * Objective: To manage Grounds for take-off delays
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa .api.entity.parametric.DelayCause;
import java.util.List;

/**
 * To manage Grounds for take-off delays
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface DelayCauseService{

    /**
     * List all entities
     * @return
     */
    List<DelayCause> findAll();

    /**
     * Save entity
     * @param delayCause
     */
    void save(DelayCause delayCause,String userName);

    /**
     * Update entity
     * @param delayCause
     */
    void update(DelayCause delayCause,String userName);

    /**
     * Delete entity
     *@param delayCause
     */
    void remove(DelayCause delayCause,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    DelayCause getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<DelayCause> getAllByFilter(String filter);

    
}
