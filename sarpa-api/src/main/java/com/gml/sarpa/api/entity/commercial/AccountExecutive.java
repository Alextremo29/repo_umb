/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AccountExecutive .java
 * Created on: Fri Jun 30 22:08:06 COT 2017
 * Project: sarpa
 * Objective: To define commercial account executives
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import com.gml.sarpa.api.entity.people.Person;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To define commercial account executives
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "account_executive", schema = "commercial", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"person_id"},name = AccountExecutive.UC_account_executive_person),
   @UniqueConstraint(columnNames = {"code"},name = AccountExecutive.UC_account_executive_code),
})
public class AccountExecutive implements Serializable{

    public static final String UC_account_executive_person =
        "UC_account_executive_person";
     public static final String UC_account_executive_code =
        "UC_account_executive_code";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
    * person
    */
    @JoinColumn(name = "person_id", nullable = false)
    @ManyToOne
    private Person person;

    /**
    * Code
    */
    @Column(name = "code", nullable = false)
    private String code;
    
    /**
    * detail
    */
    @Column(name = "detail", nullable = true)
    private String detail;

    /*
     * -----------------------------------------------------------------------
     */
    
    public AccountExecutive() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
    
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

   
    /*
     * -----------------------------------------------------------------------
     */

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        String toString =  "AccountExecutive{" + "id=" + id +
                ", detail=" + detail + 
                ", code=" + code + '}';
        
        if (null != person){
            toString = toString + "person=" + person.getCompleteName();
        }
       
        return toString;
    }
    
}
