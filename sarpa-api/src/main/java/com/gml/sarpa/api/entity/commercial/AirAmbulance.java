/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirAmbulance.java
 * Created on: 2017/09/06, 10:19:52 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author <a href="mailto:josej@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Entity
@Table(name = "airambulance", schema = "commercial", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"name"},name = AirAmbulance.UC_ambulance_group),
})
public class AirAmbulance implements Serializable{

    public static final String UC_ambulance_group =
        "UC_ambulance_group";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "generator_id")
    @SequenceGenerator(name = "generator_id", schema = "public",
        sequenceName = "hibernate_sequence")
    private Long id;

    /**
     * Object version
     */
    @Column(name = "version", nullable = false)
    private Integer version;

    /**
     * Date created
     */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;

    /**
     * Date last updated
     */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;

    /**
     * User created
     */
    @Column(name = "user_created", nullable = false)
    private String userCreated;

    /**
     * User last updated
     */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
     * name
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * description
     */
    @Column(name = "description", nullable = false)
    private String description;

    /*
     * -----------------------------------------------------------------------
     */
    public AirAmbulance() {

    }

    /*
     * -----------------------------------------------------------------------
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion + 1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public void setDateCreated(Date date) {
        this.dateCreated = date;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*
     * -----------------------------------------------------------------------
     */
    @Override
    public String toString() {
        return "AmbulanceGround {" +
            "name=" + name +
            "description=" + description +'}';
    }
}
