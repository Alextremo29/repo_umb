/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportRateService.java
 * Created on: Thu Sep 28 09:00:31 COT 2017
 * Project: sarpa
 * Objective: To manage airports rates.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.AirportRate;
import java.util.List;

/**
 * To manage airports rates
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */

public interface AirportRateService{

    /**
     * List all entities
     * @return
     */
    List<AirportRate> findAll();

    /**
     * Save entity
     * @param airportRate
     */
    void save(AirportRate airportRate,String userName);

    /**
     * Update entity
     * @param airportRate
     */
    void update(AirportRate airportRate,String userName);

    /**
     * Delete entity
     *@param airportRate
     */
    void remove(AirportRate airportRate,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    AirportRate getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<AirportRate> getAllByFilter(String filter);

    
}
