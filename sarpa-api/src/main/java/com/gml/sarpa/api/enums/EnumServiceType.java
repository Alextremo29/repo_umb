/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: EnumServiceType.java
 * Created on: 2017/09/13, 10:59:42 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.enums;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
public enum EnumServiceType {
    
    AIR_AMBULANCE("Ambulancia Aérea"), 
    ORGANS("Órganos"),
    MEDICAL_GUARD("Escolta medica"),
    GROUND_AMBULANCE("Ambulancia Terrestre"),
    HEALTH_ROUTES("Rutas de la Salud"),
    PASSENGERS("Pasajeros"),
    LOAD("Carga");
    
    private final String name;
    
	private EnumServiceType(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
}
