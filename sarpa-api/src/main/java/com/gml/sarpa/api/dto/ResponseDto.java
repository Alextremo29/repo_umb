/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gml.sarpa.api.dto;

/**
 *
 * @author angie
 */
public class ResponseDto {
    /**
     * 0: exitoso, 1: error.
     */
    private String code;
    /**
     * Mensajes de error.
     */
    private String message;
    /**
     * Data de la respuesta.
     */
    private Object data;
    /**
     * Total de filas.
     */
    private int recordsTotal;
    /**
     * Total filtradas.
     */
    private int recordsFiltered;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }
}
