/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerRequest .java
 * Created on: Wed Jul 26 17:22:44 COT 2017
 * Project: sarpa
 * Objective: To manage customers requests
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import com.gml.sarpa.api.entity.operation.Airplane;
import com.gml.sarpa.api.entity.operation.AirplaneType;
import com.gml.sarpa.api.entity.operation.Airport;
import com.gml.sarpa.api.entity.operation.Journey;
import com.gml.sarpa.api.entity.operation.LoadType;
import com.gml.sarpa.api.entity.operation.ServiceType;
import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import com.gml.sarpa.api.enums.EnumServiceType;
import com.gml.sarpa.api.enums.EnumUnitBusiness;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * To manage customers requests
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "customer_request", schema = "commercial", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"code"},name = CustomerRequest .UC_customer_request),
})
public class CustomerRequest implements Serializable{

    public static final String UC_customer_request =
        "UC_customer_request";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
    * customer
    */
    @JoinColumn(name = "customer_id", nullable = true)
    @ManyToOne
    private Customer customer;

    /**
    * airplane
    */
    @JoinColumn(name = "airplane_id", nullable = true)
    @ManyToOne
    private Airplane airplane;

    /**
    * initDate
    */
    @Column(name = "init_date", nullable = false)
    private Date initDate;

    /**
    * endDate
    */
    @Column(name = "end_date", nullable = false)
    private Date endDate;

    /**
    * serviceType
    */
    @JoinColumn(name = "service_type_id", nullable = false)
    @ManyToOne
    private ServiceType serviceType;

    /**
    * loadType
    */
    @JoinColumn(name = "load_type_id", nullable = true)
    @ManyToOne
    private LoadType loadType;
    
    /**
    * Other Load Type.
    */    
    @Column(name = "otherLoadType", nullable = true)  
    private String otherLoadType;

    /**
     * Load Type Long.
     */
    @Column(name = "loadTypeLong", nullable = true)
    private Double loadTypeLong;
  
    /**
    * code
    */
    @Column(name = "code", nullable = false)
    private String code;

    /**
    * detail
    */
    @Column(name = "detail", nullable = true)
    private String detail;

    /**
    * status
    */
    @Column(name = "status", nullable = false)
    private String status;

    /**
    * Business Unit
    */
    @JoinColumn(name = "business_unit_id", nullable = true)
    @ManyToOne
    private BusinessUnit businessUnit;
    
    /**
    * Caller
    */
    @Column(name = "caller", nullable = true)
    private String caller;
    
    /**
     * Company Service.
     */
    @JoinColumn(name = "company_id", nullable = true)
    @ManyToOne
    private Company company;
    /**
     * Probable Fligh Date
     */
    @Column(name = "probableflightdate", nullable = true)
    private String probableflightdate;
    /**
     * Probable Fligh Date
     */
    @Column(name = "probableflighthour", nullable = true)
    private String probableflighthour; 
    
    /**
    * Air Ambulance
    */
    @JoinColumn(name = "air_ambulance_id", nullable = true)
    @ManyToOne
    private AirAmbulance airAmbulance;

    /**
     * Include Rescue Equipment.
     */
    @Column(name = "includesRescueEquipment", nullable = true)
    private Boolean includesRescueEquipment;
    
    /**
     * Number Of Passengers
     */
    @Column(name = "numberOfPassengers", nullable = true)
    private Integer numberOfPassengers;
    
    /**
     * Type Of Passengers
     */
    @Column(name = "typeOfPassenger", nullable = true)
    private String typeOfPassenger;
    
    /**
     * Type of Organ.
     */
    @Column(name = "typeoforgan", nullable = true)
    private String typeoforgan;
    /**
     * Requires Ground Ambulance
     */
    @Column(name = "requiresGroundAmbulance", nullable = true)
    private Boolean requiresGroundAmbulance;
    
    /**
     * Requires Oxigen.
     */
    @Column(name = "requiresOxygen", nullable = true)
    private Boolean requiresOxygen;
    
    /**
     * Oxigen oxygenFiO2.
     */
    @Column(name = "oxygenFiO2", nullable = true)
    private String oxygenFiO2;

    /**
     * Requires Ticket
     */
    @Column(name = "requiresTicket", nullable = true)
    private Boolean requiresTicket;
    /**
     * Travel with companion
     */
    @Column(name = "travelWithCompanion", nullable = true)
    private Boolean travelWithCompanion;
    
    /**
     * Authorized Treating Doctor.
     */
    @Column(name = "authorizedTreatingDoctor", nullable = true)
    private Boolean authorizedTreatingDoctor; 

    /**
     * City of Service.
     */
    @Column(name = "cityOfService", nullable = true)
    private String cityOfService;
    /**
     * Birth Place.
     */
    @Column(name = "birthPlace", nullable = true)
    private String birthPlace;
    /**
     * Destination Place.
     */
    @Column(name = "destinationPlace", nullable = true)
    private String destinationPlace;
    
    /**
     * Configuration ground Ambulance
     */
    @JoinColumn(name = "configuration_groun_amb_id", nullable = true)
    @ManyToOne
    private GroundAmbulance configurationGA;
   
    /**
     * Type
     */
    @Column(name = "type", nullable = true)
    private String type;
    
    /**
     * Medical Crew on Board.
     */
    @Column(name = "medicalCrewOnBoard", nullable = true)
    private Boolean medicalCrewOnBoard;
    
    /**
     * Required Ground Ambulance
     */
    @Column(name = "requiredGroundAmbulance", nullable = true)
    private Boolean requiredGroundAmbulance;
    /**
     * Required Caterin Service.
     */
    @Column(name = "requiredCaterinService", nullable = true)
    private Boolean requiredCaterinService;
    
    /**
     * Load Type Long.
     */
    @Column(name = "loadTypeLongNA", nullable = true)
    private String loadTypeLongNA;
    
    /**
     * Load Type Width
     */
    @Column(name = "loadTypeWidth", nullable = true)
    private Double loadTypeWidth;
    /**
     * Load Type Long.
     */
    @Column(name = "loadTypeWidthNA", nullable = true)
    private String loadTypeWidthNA;
    /**
     * Load Type Weight Per Piece
     */
    @Column(name = "loadTypeWeightPerPiece", nullable = true)
    private Double loadTypeWeightPerPiece;
    
    /**
     * Load Type Weight Per Piece NA
     */
    @Column(name = "loadTypeWeightPerPieceNA", nullable = true)
    private String loadTypeWeightPerPieceNA;
    /**
     * Load Type High.
     */
    @Column(name = "loadTypeHigh", nullable = true)
    private Double loadTypeHigh;

    /**
     * Load Type High N.A
     */
    @Column(name = "loadTypeHighNA", nullable = true)
    private String loadTypeHighNA;
    /**
     * Shipper Declaration
     */
    @Column(name = "shipperDeclaration", nullable = true)
    private String shipperDeclaration;
    
    
   
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso1;
    
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso2;
    
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso3;  
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso4;  
   
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso5;
    
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso6;
    
    /**
     * 
     */
    @Transient
    private Boolean existeDetalleServicioEnSolicitudCaso7;
    
    /**
     * 
     */
    @Transient
    private String nameCompany;

    /**
     * Origin Airport
     */
    @JoinColumn(name = "origin_id", nullable = true)
    @ManyToOne
    private Airport origin;
    
    /**
     * Destination airport
     */
    @JoinColumn(name = "destination_id", nullable = true)
    @ManyToOne
    private Airport destination;
    
    /**
     * Airplane Type
     */
    @JoinColumn(name = "airplane_type_id", nullable = true)
    @ManyToOne
    private AirplaneType airplaneType;   
    
    /**
     * Journeys
     */
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(schema = "commercial", name = "customer_request_journey", joinColumns = {@JoinColumn(
            name = "customer_request_id")}, inverseJoinColumns = {@JoinColumn(
            name = "journey_id")})
    private Set<Journey> journeys = new HashSet();
    
    /**
     * Flight Time
     */
    @JoinColumn(name = "flight_time", nullable = true)
    private String flightTime;
    /**
     * Time to quote
     */
    @JoinColumn(name = "time_to_quote", nullable = true)
    private String timetoquote;
    /**
     * Observations
     */
    @JoinColumn(name = "observations", nullable = true)
    private String observations;
    
    /**
     * Value Caterin.
     */
    @JoinColumn(name = "value_caterin", nullable = true)
    private BigDecimal valueCaterin;
    /**
     * Required Airport Rates.
     */
    @JoinColumn(name = "required_airport_rates", nullable = true)
    private Boolean requiredAirportRates;
    
    /**
     * Value Airport Rates.
     */
    @JoinColumn(name = "value_airport_rates", nullable = true)
    private BigDecimal valueAirportRates;
    
    /**
     * Required Extension Hour.
     */
    @JoinColumn(name = "required_airport_ext", nullable = true)
    private Boolean requiredAirportExtension;
    /**
     * Value Airport Extension.
     */
    @JoinColumn(name = "value_airport_ext", nullable = true)
    private BigDecimal valueAirportExtension;
    
    /**
     * Required FBO.
     */
    @JoinColumn(name = "required_fbo", nullable = true)
    private Boolean requiredFBO;
    /**
     * Value FBO.
     */
    @JoinColumn(name = "value_fbo", nullable = true)
    private BigDecimal valueFBO;
    /**
     * Value Ground Ambulance.
     */
    @JoinColumn(name = "value_ground_ambulance", nullable = true)
    private BigDecimal valueGroundAmbulance;
    /**
     * Value Flight Time to Quote.
     */
    @JoinColumn(name = "value_flightime_quote", nullable = true)
    private BigDecimal valueFlighttimeQuote;
    
    /**
     * Required Special Configuration.
     */
    @JoinColumn(name = "required_special_config", nullable = true)
    private Boolean requiredSpecialConfiguration;
    /**
     * Value Special Configuration.
     */
    @JoinColumn(name = "value_special_config", nullable = true)
    private BigDecimal valueSpecialConfiguration;
    
    /**
     * Other Services.
     */
    @JoinColumn(name = "other_services", nullable = true)
    private String otherServices;
    
    /**
     * Value Other Services.
     */
    @JoinColumn(name = "value_other_services", nullable = true)
    private BigDecimal valueOtherServices;
    
    /*
     * -----------------------------------------------------------------------
     */
    public CustomerRequest() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public void setDateCreated(Date date) {
        this.dateCreated = date;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public LoadType getLoadType() {
        return loadType;
    }

    public void setLoadType(LoadType loadType) {
        this.loadType = loadType;
    }
    
    public String getOtherLoadType() {
        return otherLoadType;
    }

    public void setOtherLoadType(String otherLoadType) {
        this.otherLoadType = otherLoadType;
    }
    
    public Double getLoadTypeLong() {
        return loadTypeLong;
    }

    public void setLoadTypeLong(Double loadTypeLong) {
        this.loadTypeLong = loadTypeLong;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BusinessUnit getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(BusinessUnit businessUnit) {
        this.businessUnit = businessUnit;
    }
    
    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public String getProbableflightdate() {
        return probableflightdate;
    }

    public void setProbableflightdate(String probableflightdate) {
        this.probableflightdate = probableflightdate;
    }

    public String getProbableflighthour() {
        return probableflighthour;
    }

    public void setProbableflighthour(String probableflighthour) {
        this.probableflighthour = probableflighthour;
    }
    
    public AirAmbulance getAirAmbulance() {
        return airAmbulance;
    }

    public void setAirAmbulance(AirAmbulance airAmbulance) {
        this.airAmbulance = airAmbulance;
    }
     
    public Boolean getIncludesRescueEquipment() {
        return includesRescueEquipment;
    }

    public void setIncludesRescueEquipment(Boolean includesRescueEquipment) {
        this.includesRescueEquipment = includesRescueEquipment;
    }

    public Integer getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(Integer numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }
    
    public String getTypeOfPassenger() {
        return typeOfPassenger;
    }

    public void setTypeOfPassenger(String typeOfPassenger) {
        this.typeOfPassenger = typeOfPassenger;
    }
    
    public String getTypeoforgan() {
        return typeoforgan;
    }

    public void setTypeoforgan(String typeoforgan) {
        this.typeoforgan = typeoforgan;
    }

    public Boolean getRequiresGroundAmbulance() {
        return requiresGroundAmbulance;
    }

    public void setRequiresGroundAmbulance(Boolean requiresGroundAmbulance) {
        this.requiresGroundAmbulance = requiresGroundAmbulance;
    }
    
    public Boolean getRequiresOxygen() {
        return requiresOxygen;
    }

    public void setRequiresOxygen(Boolean requiresOxygen) {
        this.requiresOxygen = requiresOxygen;
    }
    
    public String getOxygenFiO2() {
        return oxygenFiO2;
    }

    public void setOxygenFiO2(String oxygenFiO2) {
        this.oxygenFiO2 = oxygenFiO2;
    }

    public Boolean getRequiresTicket() {
        return requiresTicket;
    }

    public void setRequiresTicket(Boolean requiresTicket) {
        this.requiresTicket = requiresTicket;
    }

    public Boolean getTravelWithCompanion() {
        return travelWithCompanion;
    }

    public void setTravelWithCompanion(Boolean travelWithCompanion) {
        this.travelWithCompanion = travelWithCompanion;
    }

    public Boolean getAuthorizedTreatingDoctor() {
        return authorizedTreatingDoctor;
    }

    public void setAuthorizedTreatingDoctor(Boolean authorizedTreatingDoctor) {
        this.authorizedTreatingDoctor = authorizedTreatingDoctor;
    }
    
    public String getCityOfService() {
        return cityOfService;
    }

    public void setCityOfService(String cityOfService) {
        this.cityOfService = cityOfService;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getDestinationPlace() {
        return destinationPlace;
    }

    public void setDestinationPlace(String destinationPlace) {
        this.destinationPlace = destinationPlace;
    }
    
    public GroundAmbulance getConfigurationGA() {
        return configurationGA;
    }

    public void setConfigurationGA(GroundAmbulance configurationGA) {
        this.configurationGA = configurationGA;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public Boolean getMedicalCrewOnBoard() {
        return medicalCrewOnBoard;
    }

    public void setMedicalCrewOnBoard(Boolean medicalCrewOnBoard) {
        this.medicalCrewOnBoard = medicalCrewOnBoard;
    }

    public Boolean getRequiredGroundAmbulance() {
        return requiredGroundAmbulance;
    }

    public void setRequiredGroundAmbulance(Boolean requiredGroundAmbulance) {
        this.requiredGroundAmbulance = requiredGroundAmbulance;
    }

    public Boolean getRequiredCaterinService() {
        return requiredCaterinService;
    }

    public void setRequiredCaterinService(Boolean requiredCaterinService) {
        this.requiredCaterinService = requiredCaterinService;
    }
    
    public String getLoadTypeLongNA() {
        return loadTypeLongNA;
    }

    public void setLoadTypeLongNA(String loadTypeLongNA) {
        this.loadTypeLongNA = loadTypeLongNA;
    }

    public Double getLoadTypeWidth() {
        return loadTypeWidth;
    }

    public void setLoadTypeWidth(Double loadTypeWidth) {
        this.loadTypeWidth = loadTypeWidth;
    }

    public String getLoadTypeWidthNA() {
        return loadTypeWidthNA;
    }

    public void setLoadTypeWidthNA(String loadTypeWidthNA) {
        this.loadTypeWidthNA = loadTypeWidthNA;
    }

    public Double getLoadTypeWeightPerPiece() {
        return loadTypeWeightPerPiece;
    }

    public void setLoadTypeWeightPerPiece(Double loadTypeWeightPerPiece) {
        this.loadTypeWeightPerPiece = loadTypeWeightPerPiece;
    }

    public String getLoadTypeWeightPerPieceNA() {
        return loadTypeWeightPerPieceNA;
    }

    public void setLoadTypeWeightPerPieceNA(String loadTypeWeightPerPieceNA) {
        this.loadTypeWeightPerPieceNA = loadTypeWeightPerPieceNA;
    }

    public Double getLoadTypeHigh() {
        return loadTypeHigh;
    }

    public void setLoadTypeHigh(Double loadTypeHigh) {
        this.loadTypeHigh = loadTypeHigh;
    }

    public String getLoadTypeHighNA() {
        return loadTypeHighNA;
    }

    public void setLoadTypeHighNA(String loadTypeHighNA) {
        this.loadTypeHighNA = loadTypeHighNA;
    }

    public String getShipperDeclaration() {
        return shipperDeclaration;
    }

    public void setShipperDeclaration(String shipperDeclaration) {
        this.shipperDeclaration = shipperDeclaration;
    }
    
    public BigDecimal getValueCaterin() {
        return valueCaterin;
    }

    public void setValueCaterin(BigDecimal valueCaterin) {
        this.valueCaterin = valueCaterin;
    }

    public Boolean getRequiredAirportRates() {
        return requiredAirportRates;
    }

    public void setRequiredAirportRates(Boolean requiredAirportRates) {
        this.requiredAirportRates = requiredAirportRates;
    }

    public BigDecimal getValueAirportRates() {
        return valueAirportRates;
    }

    public void setValueAirportRates(BigDecimal valueAirportRates) {
        this.valueAirportRates = valueAirportRates;
    }

    public Boolean getRequiredAirportExtension() {
        return requiredAirportExtension;
    }

    public void setRequiredAirportExtension(Boolean requiredAirportExtension) {
        this.requiredAirportExtension = requiredAirportExtension;
    }

    public BigDecimal getValueAirportExtension() {
        return valueAirportExtension;
    }

    public void setValueAirportExtension(BigDecimal valueAirportExtension) {
        this.valueAirportExtension = valueAirportExtension;
    }

    public Boolean getRequiredFBO() {
        return requiredFBO;
    }

    public void setRequiredFBO(Boolean requiredFBO) {
        this.requiredFBO = requiredFBO;
    }

    public BigDecimal getValueFBO() {
        return valueFBO;
    }

    public void setValueFBO(BigDecimal valueFBO) {
        this.valueFBO = valueFBO;
    }
    
    public BigDecimal getValueGroundAmbulance() {
        return valueGroundAmbulance;
    }

    public void setValueGroundAmbulance(BigDecimal valueGroundAmbulance) {
        this.valueGroundAmbulance = valueGroundAmbulance;
    }
    
    public BigDecimal getValueFlighttimeQuote() {
        return valueFlighttimeQuote;
    }

    public void setValueFlighttimeQuote(BigDecimal valueFlighttimeQuote) {
        this.valueFlighttimeQuote = valueFlighttimeQuote;
    }

    
    public Boolean getRequiredSpecialConfiguration() {
        return requiredSpecialConfiguration;
    }

    public void setRequiredSpecialConfiguration(Boolean requiredSpecialConfiguration) {
        this.requiredSpecialConfiguration = requiredSpecialConfiguration;
    }

    public BigDecimal getValueSpecialConfiguration() {
        return valueSpecialConfiguration;
    }

    public void setValueSpecialConfiguration(BigDecimal valueSpecialConfiguration) {
        this.valueSpecialConfiguration = valueSpecialConfiguration;
    }

    public String getOtherServices() {
        return otherServices;
    }

    public void setOtherServices(String otherServices) {
        this.otherServices = otherServices;
    }

    public BigDecimal getValueOtherServices() {
        return valueOtherServices;
    }

    public void setValueOtherServices(BigDecimal valueOtherServices) {
        this.valueOtherServices = valueOtherServices;
    }
 
    @Transient
    public Boolean getExisteDetalleServicioEnSolicitudCaso1() {

        existeDetalleServicioEnSolicitudCaso1 =
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.AMBULANCE.getName(),
                EnumServiceType.AIR_AMBULANCE.getName());
        return existeDetalleServicioEnSolicitudCaso1;
    }

    public void setExisteDetalleServicioEnSolicitudCaso1(
        Boolean existeDetalleServicioEnSolicitudCaso1) {
        this.existeDetalleServicioEnSolicitudCaso1 =
            existeDetalleServicioEnSolicitudCaso1;
    }
    
    @Transient
    public Boolean getExisteDetalleServicioEnSolicitudCaso2() {
        existeDetalleServicioEnSolicitudCaso2 =
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.AMBULANCE.getName(), 
                EnumServiceType.ORGANS.getName());
        return existeDetalleServicioEnSolicitudCaso2;
    }

    public void setExisteDetalleServicioEnSolicitudCaso2(
        Boolean existeDetalleServicioEnSolicitudCaso2) {
        this.existeDetalleServicioEnSolicitudCaso2 =
            existeDetalleServicioEnSolicitudCaso2;
    }  
    
    @Transient
    public Boolean getExisteDetalleServicioEnSolicitudCaso3() {
        existeDetalleServicioEnSolicitudCaso3 =
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.AMBULANCE.getName(), 
                EnumServiceType.MEDICAL_GUARD.getName());
        return existeDetalleServicioEnSolicitudCaso3;
    }

    public void setExisteDetalleServicioEnSolicitudCaso3(
        Boolean existeDetalleServicioEnSolicitudCaso3) {
        this.existeDetalleServicioEnSolicitudCaso3 =
            existeDetalleServicioEnSolicitudCaso3;
    }
    
    @Transient
    public Boolean getExisteDetalleServicioEnSolicitudCaso4() {
        existeDetalleServicioEnSolicitudCaso4 =
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.AMBULANCE.getName(),
                EnumServiceType.GROUND_AMBULANCE.getName());
        return existeDetalleServicioEnSolicitudCaso4;
    }

    public void setExisteDetalleServicioEnSolicitudCaso4(
        Boolean existeDetalleServicioEnSolicitudCaso4) {
        this.existeDetalleServicioEnSolicitudCaso4 =
            existeDetalleServicioEnSolicitudCaso4;
    }
    
    @Transient
    public Boolean getExisteDetalleServicioEnSolicitudCaso5() {
        existeDetalleServicioEnSolicitudCaso5 =
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.AMBULANCE.getName(), 
                EnumServiceType.HEALTH_ROUTES.getName());
        return existeDetalleServicioEnSolicitudCaso5;
    }

    public void setExisteDetalleServicioEnSolicitudCaso5(
        Boolean existeDetalleServicioEnSolicitudCaso5) {
        this.existeDetalleServicioEnSolicitudCaso5 =
            existeDetalleServicioEnSolicitudCaso5;
    }
    
    @Transient
    public Boolean getExisteDetalleServicioEnSolicitudCaso6() {
        existeDetalleServicioEnSolicitudCaso6 = 
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.CHARTER.getName(), 
                EnumServiceType.PASSENGERS.getName());    
        return existeDetalleServicioEnSolicitudCaso6;
    }

    public void setExisteDetalleServicioEnSolicitudCaso6(
        Boolean existeDetalleServicioEnSolicitudCaso6) {
        this.existeDetalleServicioEnSolicitudCaso6 =
            existeDetalleServicioEnSolicitudCaso6;
    }
    
    public Boolean getExisteDetalleServicioEnSolicitudCaso7() {
        existeDetalleServicioEnSolicitudCaso7 = 
            validarDetalleServicioEnSolicitudCaso(EnumUnitBusiness.CHARTER.getName(), 
                EnumServiceType.LOAD.getName()); 
        return existeDetalleServicioEnSolicitudCaso7;
    }

    public void setExisteDetalleServicioEnSolicitudCaso7(
        Boolean existeDetalleServicioEnSolicitudCaso7) {
        this.existeDetalleServicioEnSolicitudCaso7 =
            existeDetalleServicioEnSolicitudCaso7;
    }
    
    @Transient
    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }
    
    public Airport getOrigin() {
        return origin;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }
    
    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Set<Journey> getJourneys() {
        return journeys;
    }

    public void setJourneys(Set<Journey> journeys) {
        this.journeys = journeys;
    }
    
    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }

    public String getTimetoquote() {
        return timetoquote;
    }

    public void setTimetoquote(String timetoquote) {
        this.timetoquote = timetoquote;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }
    
    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Request {" + 
            "initDate=" + initDate+
            "endDate=" + endDate+
            "serviceType=" + serviceType.getName()+
            "status=" + status+
            "business unit=" + businessUnit.getName()
        + '}';
    }
    
    private Boolean validarDetalleServicioEnSolicitudCaso(
        String businessUnit, String serviceType) {

        boolean isCaseValidate = false;
        final boolean fieldsValidate = this.businessUnit != null &&
            this.businessUnit.getName() != null && this.serviceType != null
            && this.serviceType.getName() != null;
        if (fieldsValidate) {
            businessUnit = businessUnit.trim().toUpperCase();
            serviceType = serviceType.trim().toUpperCase();
            final String businessUnitRegister = this.businessUnit.getName().trim().toUpperCase();
            final String serviceTypeRegister = this.serviceType.getName().trim().toUpperCase();
            isCaseValidate =
                businessUnitRegister.contains(businessUnit) &&
                 serviceTypeRegister.contains(serviceType);
        }
        return isCaseValidate;
    }
 
}
