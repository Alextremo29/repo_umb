/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gml.sarpa.exception.code;

/**
 *
 * @author angie
 */
public enum ErrorCode {

    ERR_001_UNKNOW("Error 001: Ha ocurrido un error inesperado. " +
        "Comuníquese con el administrador."),
    ERR_002_IDENTIFICATION_EXIST("Error 002: Identificación del cliente ya " +
        "se encuentra registrada."),
    ERR_003_TWITTER_EXIST("Error 003: Cuenta de Twitter ya se encuentra " +
        "registrada."),
    ERR_004_SELECT_DOCUMENT_TYPE("Error 004: Seleccione tipo de documento."),
    ERR_005_EMAIL_EXIST("Error 005: Email ya se encuentra registrado.");

    private final String message;

    private ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
