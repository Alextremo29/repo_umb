﻿--ROLES CREATION
----Develop
CREATE ROLE gml_sarpa_develop_user WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	REPLICATION
	CONNECTION LIMIT -1
	PASSWORD '1234567890';

----Test
----Production

--DATABASES CREATION
----Develop
CREATE DATABASE gml_sarpa_develop
    WITH 
    OWNER = gml_sarpa_develop_user
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

----Test
----Production
