# README 

Application for SARPA operation

## CONFIGURATION OF DEVELOPMENT ENVIRONMENT

The environment is configured in a windows environment.

### DOWNLOAD AND INSTALL APPLICATIONS REQUIRED

1. 	Install jdk 1.8: [https://www.java.com/es/download/](https://www.java.com/es/download/ "Go").

2. 	Install Netbeans Ide: [https://netbeans.org/downloads/](https://netbeans.org/downloads/ "Go").

3. 	Install Git: [https://git-scm.com/download/win](https://git-scm.com/download/win "Descargar instalador"). [Recommended Tutorial](http://learngitbranching.js.org/ "Go").

4. 	Install Postgresql 9.5.6: [https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows "Go"). 

5.  Download and unzip Apache Tomcat: [https://tomcat.apache.org/download-80.cgi](https://tomcat.apache.org/download-80.cgi "Go"). 

6. 	Download postgresql driver: [https://jdbc.postgresql.org/download.html](https://jdbc.postgresql.org/download.html "Go") 

7.  Copy postgres driver into glassfish folder (step 5) apache-tomcat-8.5.16\lib

### DATABASE PREPARATION

1.  Run script BD/1-database-inicialization.sql

2.  Restore backup BD/2-database-population.backup


### DEVELOP SERVER PREPARATION (Netbeans)
 
1. Check correct JavaBD ( netbeans/databases) configuration 

2. Remove Apache tomcat server in netbeans

3. Create new Apache tomcat server in neatbens using downloaded version

4. Edit file apache-tomcat-8.5.16\conf\context.xml. Set resource info with database conection information and location of config files. Add these code:

        <Resource name="jdbc/sarpa" auth="Container"
              type="javax.sql.DataSource" driverClassName="org.postgresql.Driver"
              url="jdbc:postgresql://localhost:5432/gml_sarpa_develop"
              username="gml_sarpa_develop_user" password="1234567890" maxTotal="20" maxIdle="10"
              maxWaitMillis="-1"/>
			  
	<Environment name="APP_CONFIG" value="C:\config" type="java.lang.String"/>

5. Set user admin and passowrd 1234567890



### APPLICATION PREPARE
 
1. 	Configure Netbeans template.

2.  Checkout Git project in Netbeans: 

		* Team/git/clone.

		* Clone from bitbucket https://usuario@bitbucket.org/gmlproducts/repo_da.git

        * Configuring git access data per terminal:

            * cd C:Glm/Proyectos/simpc/develop/repo_simpc/
            * git config --global user.name "YOUR NAME"
            * git config --global user.email "YOUR EMAIL ADDRESS"

3. 	In web project properties, configure Apache tomcat server for Run. Define Context Path /sarpa