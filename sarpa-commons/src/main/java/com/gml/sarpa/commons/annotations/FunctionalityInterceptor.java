/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FunctionalityInterceptor.java
 * Created on: 2016/12/05, 04:23:22 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 *
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.commons.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

/**
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;nez</a>
 */
@Target({TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FunctionalityInterceptor {

    /**
     * Nombre de la funcionalidad.
     *
     * @return
     */
    String[] name();
}
