/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: Util.java
 * Created on: 2017/04/28, 10:36:42 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.commons.util;

import org.apache.log4j.Logger;


/**
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
public class Util {
    
    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(com.gml.sarpa.api.utilities.Util.class);
    
    
}
