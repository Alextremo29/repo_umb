/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MinuteValueController.java
 * Created on: Fri Jul 21 10:43:45 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.web.controller.ControllerDefinition;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.MinuteValue;
import com.gml.sarpa.api.utilities.Util;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import com.gml.sarpa.api.services.commercial.MinuteValueService;
import com.gml.sarpa.api.services.operation.AirplaneTypeService;
import com.gml.sarpa.api.services.operation.ServiceTypeService;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class MinuteValueController implements ControllerDefinition {

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(MinuteValueController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private MinuteValueService entityService;

    @Autowired
    private ServiceTypeService serviceTypeService;

    @Autowired
    private AirplaneTypeService airplaneTypeService;

    @Autowired
    private MessageSource messageSource;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/minuteValue-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes) {

        LOGGER.info("Runing [ JourneyValueController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("commercial/minuteValue/index");
        init(request);

        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType", flashMap.get("flashType"));
            modelAndView.addObject("flashMessage", flashMap.get("flashMessage"));
        }

        modelAndView.addObject("indexList", this.entityService.findAll());

        return modelAndView;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/minuteValue-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session) {

        LOGGER.info("Runing [ JourneyValueController - create ]");
        ModelAndView modelAndView = new ModelAndView("commercial/minuteValue/create");
        init(request);

        MinuteValue minuteValue = new MinuteValue();
        modelAndView.addObject("minuteValue", minuteValue);
        modelAndView.addObject("airplaneTypeList", airplaneTypeService.findAll());
        modelAndView.addObject("serviceTypeList", serviceTypeService.findAll());
        return modelAndView;
    }

    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/minuteValue-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("minuteValue") MinuteValue minuteValue) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ JourneyValueController - save ]");
        init(request);
        try {
            entityService.save(minuteValue, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.minuteValue.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.minuteValue.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/minuteValue-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ JourneyValueController - edit ] id " + id);
        ModelAndView modelAndView = new ModelAndView("commercial/minuteValue/edit");
        init(request);

        MinuteValue minuteValue = entityService.getOne(id);

        if (minuteValue != null) {
            modelAndView.addObject("minuteValue", minuteValue);
            modelAndView.addObject("airplaneTypeList", airplaneTypeService.findAll());
            modelAndView.addObject("serviceTypeList", serviceTypeService.findAll());
        } else {
            redirectAttributes.addFlashAttribute("flashType", "danger");
            redirectAttributes.addFlashAttribute("flashMessage", "commercial.minuteValue.edit.success");
            return new ModelAndView("redirect:/minuteValue-index.htm");
        }
        return modelAndView;

    }

    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/minuteValue-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("minuteValue") MinuteValue minuteValue) {

        LOGGER.info("Runing [ JourneyValueController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(minuteValue, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.minuteValue.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.minuteValue.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/minuteValue-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ JourneyValueController - delete ] id " + id);
        init(request);

        MinuteValue object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "commercial.minuteValue.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "commercial.minuteValue.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("commercial.minuteValue.delete.error");
        }
        return Util.getJson(result);

    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
