/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportRateController.java
 * Created on: Thu Sep 26 14:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.parametric;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.parametric.AirportRate;
import com.gml.sarpa.api.services.operation.AirportService;
import com.gml.sarpa.api.services.parametric.CurrencyService;
import com.gml.sarpa.api.services.parametric.AirportRateService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;


/**
 * To manage fbo costs
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Controller
public class AirportRateController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(AirportRateController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private AirportRateService entityService;

    @Autowired
    private MessageSource messageSource;
    
    /**
     * AirplaneService service
     */
    @Autowired
    private AirportService airPortService;
    
    /**
     * CurrencyService service
     */
    @Autowired
    private CurrencyService currencyService;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/airportrate-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ AirportRateController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("parametric/airportRate/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/airportrate-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ AirportRateController - create ]");
        ModelAndView modelAndView = new ModelAndView("parametric/airportRate/create");
        init(request);
        
        AirportRate airportRate = new AirportRate();
        modelAndView.addObject("airportRate", airportRate);
        modelAndView.addObject("currencyList", currencyService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/airportrate-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("airportRate") AirportRate airportRate) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ AirportRateController - save ]");
        init(request);
        try {
            entityService.save(airportRate, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.airportrate.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.airportrate.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value ="/airportrate-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ AirportRateController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("parametric/airportRate/edit");
        init(request);
        
        AirportRate airportRate = entityService.getOne(id);

        if (airportRate != null){
            modelAndView.addObject("airportRate", airportRate);
            modelAndView.addObject("currencyList", currencyService.findAll());
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","parametric.airportrate.edit.success");
            return new ModelAndView("redirect:/airportrate-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/airportrate-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("airportRate") AirportRate airportRate) {

        LOGGER.info("Runing [ AirportRateController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(airportRate, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.airportrate.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.airportrate.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/airportrate-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ AirportRateController - delete ] id "+id);
        init(request);
        
        AirportRate object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "parametric.airportrate.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "parametric.airportrate.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("parametric.airportrate.delete.error");
        }
        return Util.getJson(result);
        
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
