/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BusinessUnitServiceImpl .java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.parametric.BusinessUnitService;
import com.gml.sarpa.web.repository.parametric.BusinessUnitRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define business units
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "BusinessUnitService")
public class BusinessUnitServiceImpl implements BusinessUnitService{

    /**
     * Repository for data access
     */
    @Autowired
    private BusinessUnitRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    

    /**
     * List all entities
     * @return
     */
    @Override
    public List<BusinessUnit> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param businessUnit
     */
    @Override
    public void save(BusinessUnit businessUnit,String userName){
        businessUnit.setVersion(0);
        businessUnit.setUserCreated(userName);
        businessUnit.setDateCreated();
        businessUnit.setDateLastUpdated();
        businessUnit.setUserLastUpdated(userName);
        repository.save(businessUnit);
        auditLogService.auditInsert(businessUnit.getClass().getName(),
                businessUnit.getId(),businessUnit.getVersion(),
                businessUnit.toString(),userName);
    }

    /**
     * Update entity
     * @param businessUnit
     */
    @Override
    public void update(BusinessUnit businessUnit,String userName){
        BusinessUnit currentBusinessUnit = getOne(businessUnit.getId());
        businessUnit.setVersion(currentBusinessUnit.getVersion());
        businessUnit.setUserLastUpdated(userName);
        businessUnit.setDateLastUpdated();
        repository.save(businessUnit);
        if(!currentBusinessUnit.getName().equals(businessUnit.getName())){
            auditLogService.auditUpdate(businessUnit.getClass().getName(),
            businessUnit.getId(),businessUnit.getVersion(),
            currentBusinessUnit.toString(),"name",businessUnit.getName(),userName);
        }
        if(!currentBusinessUnit.getDescription().equals(businessUnit.getDescription())){
            auditLogService.auditUpdate(businessUnit.getClass().getName(),
            businessUnit.getId(),businessUnit.getVersion(),
            currentBusinessUnit.toString(),"description",businessUnit.getDescription(),userName);
        }
    }

    /**
     * Delete entity
     *@param businessUnit
     */
    @Override
    public void remove(BusinessUnit businessUnit,String userName){
        repository.delete(businessUnit);
        auditLogService.auditDelete(businessUnit.getClass().getName(),
                businessUnit.getId(),businessUnit.getVersion(),
                businessUnit.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public BusinessUnit getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<BusinessUnit> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
