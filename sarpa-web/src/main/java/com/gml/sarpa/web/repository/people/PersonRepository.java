/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PersonRepository.java
 * Created on: Fri Jun 30 22:34:42 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.people;

import com.gml.sarpa.api.entity.people.Person;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface PersonRepository extends JpaRepository<Person, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from people.person  where name ilike %?1% ", nativeQuery = true)
    public List<Person> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from people.person where id = ?1 ", nativeQuery = true)
    public Person get(Long id);
}
