/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractTypeController.java
 * Created on: Thu Aug 17 22:44:19 COT 2017
 * Project: sarpa
 * Objective: To manage clients contract types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.web.controller.ControllerDefinition;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.ContractType;
import com.gml.sarpa.api.services.commercial.ContractTypeService;
import com.gml.sarpa.api.utilities.Util;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseBody;
import com.gml.sarpa.api.dto.ResponseDto;

/**
 * To manage clients contract types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class ContractTypeController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(ContractTypeController.class);

    /**
     * Session user information
     */
    private Session userSession;

    /**
     * ContractType service
     */
    @Autowired
    private ContractTypeService entityService;

    @Autowired
    private MessageSource messageSource;


    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/contractType-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ ContractTypeController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("commercial/contractType/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/contractType-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ ContractTypeController - create ]");
        ModelAndView modelAndView = new ModelAndView("commercial/contractType/create");
        init(request);
        
        ContractType contractType = new ContractType();
        modelAndView.addObject("contractType", contractType);
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/contractType-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("contractType") ContractType contractType) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ $entityNameController - save ]");
        init(request);
        try {
            entityService.save(contractType, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.contractType.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.contractType.save.error", null, locale));
        }
        return Util.getJson(result);
    }
    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/contractType-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ ContractTypeController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("commercial/contractType/edit");
        init(request);
        
        ContractType contractType = entityService.getOne(id);

        if (contractType != null){
            modelAndView.addObject("contractType", contractType);
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","commercial.contractType.edit.success");
            return new ModelAndView("redirect:/contractType-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/contractType-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("contractType") ContractType contractType) {

        LOGGER.info("Runing [ $entityNameController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(contractType, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.contractType.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.contractType.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/contractType-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ ContractTypeController - delete ] id "+id);
        ModelAndView modelAndView = new ModelAndView("redirect:/contractType-index.htm");
        init(request);
        
        ContractType object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "commercial.contractType.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "commercial.contractType.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("commercial.$entityNameView.delete.error");
        }
        return Util.getJson(result);

    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
