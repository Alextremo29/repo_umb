/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AccountExecutiveServiceImpl .java
 * Created on: Fri Jun 30 22:08:06 COT 2017
 * Project: sarpa
 * Objective: To define commercial account executives
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.AccountExecutive;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.commercial.AccountExecutiveService;
import com.gml.sarpa.web.repository.commercial.AccountExecutiveRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define commercial account executives
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "AccountExecutiveService")
public class AccountExecutiveServiceImpl implements AccountExecutiveService{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(AccountExecutiveServiceImpl.class);
    
    /**
     * Repository for data access
     */
    @Autowired
    private AccountExecutiveRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    /**
     * List all entities
     * @return
     */
    @Override
    public List<AccountExecutive> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param accountExecutive
     */
    @Override
    public void save(AccountExecutive accountExecutive,String userName){
        accountExecutive.setVersion(0);
        accountExecutive.setUserCreated(userName);
        accountExecutive.setDateCreated();
        accountExecutive.setUserLastUpdated(userName);
        accountExecutive.setDateLastUpdated();
        accountExecutive.setCode("EC-"+repository.getNextCode());
        repository.save(accountExecutive);
        auditLogService.auditInsert(accountExecutive.getClass().getName(),
                accountExecutive.getId(),accountExecutive.getVersion(),
                accountExecutive.toString(),userName);
    }

    /**
     * Update entity
     * @param accountExecutive
     */
    @Override
    public void update(AccountExecutive accountExecutive,String userName){
        AccountExecutive currentExecutive = getOne(accountExecutive.getId());
        accountExecutive.setVersion(currentExecutive.getVersion());
        accountExecutive.setUserLastUpdated(userName);
        accountExecutive.setDateLastUpdated();
        repository.save(accountExecutive);
        if(!currentExecutive.getPerson().equals(accountExecutive.getPerson())){
            auditLogService.auditUpdate(accountExecutive.getClass().getName(),
            accountExecutive.getId(),accountExecutive.getVersion(),
            currentExecutive.toString(),"person",accountExecutive.getPerson().getCompleteName(),userName);
        }
    }

    /**
     * Delete entity
     *@param accountExecutive
     */
    @Override
    public void remove(AccountExecutive accountExecutive,String userName){
        repository.delete(accountExecutive);
        auditLogService.auditDelete(accountExecutive.getClass().getName(),
                accountExecutive.getId(),accountExecutive.getVersion(),
                accountExecutive.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public AccountExecutive getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<AccountExecutive> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
    
}
