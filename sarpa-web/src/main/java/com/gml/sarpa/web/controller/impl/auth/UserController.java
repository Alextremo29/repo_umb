/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserController.java
 * Created on: Mon Jul 17 23:14:05 COT 2017
 * Project: sarpa
 * Objective: To manage users
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.auth;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.web.controller.ControllerDefinition;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.auth.User;
import com.gml.sarpa.api.services.auth.UserService;
import com.gml.sarpa.api.services.people.PersonService;
import com.gml.sarpa.api.utilities.Util;
import org.springframework.context.MessageSource;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * To manage users
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class UserController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private UserService entityService;
    
    @Autowired
    private PersonService personService;

    @Autowired
    private MessageSource messageSource;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/user-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ UserController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("auth/user/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/user-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ UserController - create ]");
        ModelAndView modelAndView = new ModelAndView("auth/user/create");
        init(request);
        
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.addObject("personList", personService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/user-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("user") User user) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ UserController - save ]");
        init(request);
        try {
            entityService.save(user, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "auth.user.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "auth.user.save.error", null, locale));
        }
        return Util.getJson(result);
    }
    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/user-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ UserController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("auth/user/edit");
        init(request);
        
        User user = entityService.getOne(id);

        if (user != null){
            modelAndView.addObject("user", user);
            modelAndView.addObject("personList", personService.findAll());
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","auth.user.edit.success");
            return new ModelAndView("redirect:/user-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/user-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("user") User user) {

        LOGGER.info("Runing [ UserController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(user, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "auth.user.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "auth.user.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/user-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ UserController - delete ] id "+id);
        init(request);
        
        User object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "auth.user.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "auth.user.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("auth.user.delete.error");
        }
        return Util.getJson(result);
        
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
