/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeePositionRepository .java
 * Created on: Wed Jul 19 12:45:39 COT 2017
 * Project: sarpa
 * Objective: To manage employees positions
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa .api.entity.parametric.EmployeePosition;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage employees positions
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface EmployeePositionRepository extends JpaRepository<EmployeePosition, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from parametric.employee_position  where name ilike %?1% ", nativeQuery = true)
    public List<EmployeePosition> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from parametric.employee_position where id = ?1 ", nativeQuery = true)
    public EmployeePosition get(Long id);
}
