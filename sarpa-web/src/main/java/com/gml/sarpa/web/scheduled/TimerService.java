/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TimerService.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: DA - D�bito autom�tico.
 * Objective: Scheduled services for timer.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.scheduled;

import com.gml.sarpa.api.services.auth.SessionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Scheduled services for timer
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Service
public class TimerService {

    /**
     * Loger for class
     */
    private static final Logger LOGGER = Logger.getLogger(TimerService.class);
    
    /**
     * Service for manage sessions.
     */
    @Autowired
    private SessionService sessionService;
    

    /**
     * Method for delete sessions recors
     * (each tree minutes)
     */
    @Scheduled(fixedRate = 180000)
    public void expirateSession() {
        LOGGER.info("Executing  [ TimerService - expirateSession ]");

        try {
            this.sessionService.expireSessions();
        } catch (Exception e) {
            LOGGER.error("ERROR expirating sessions.", e);
        } finally {
            LOGGER.info("Sessions expiration finished.");
        }
    }

    

    
}
