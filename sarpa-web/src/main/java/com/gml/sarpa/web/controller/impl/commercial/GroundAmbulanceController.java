/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: GroundAmbulanceController.java
 * Created on: 2017/09/06, 10:56:09 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.GroundAmbulance;
import com.gml.sarpa.api.services.commercial.GroundAmbulanceService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Controller
public class GroundAmbulanceController implements ControllerDefinition {
    
    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(GroundAmbulanceController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private GroundAmbulanceService groundAmbulanceService;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/groundAmbulance-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes) {

        LOGGER.info("Runing [ GroundAmbulanceController - onDisplay ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("commercial/groundAmbulance/index");

        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType", flashMap.get("flashType"));
            modelAndView.addObject("flashMessage", flashMap.get("flashMessage"));
        }

        modelAndView.addObject("indexList", this.groundAmbulanceService.findAll());

        return modelAndView;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/groundAmbulance-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session) {

        LOGGER.info("Runing [ GroundAmbulanceController - create ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("commercial/groundAmbulance/create");

        GroundAmbulance groundAmbulance = new GroundAmbulance();
        modelAndView.addObject("groundAmbulance", groundAmbulance);
        return modelAndView;
    }

    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/groundAmbulance-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("groundAmbulance") GroundAmbulance groundAmbulance) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ GroundAmbulanceController - save ]");
        init(request);
        try {
            groundAmbulanceService.save(groundAmbulance, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.groundAmbulance.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.groundAmbulance.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/groundAmbulance-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ GroundAmbulanceController - edit ] id " + id);
        ModelAndView modelAndView = new ModelAndView("commercial/groundAmbulance/edit");
        init(request);
        GroundAmbulance groundAmbulance = groundAmbulanceService.getOne(id);

        if (groundAmbulance != null) {
            modelAndView.addObject("groundAmbulance", groundAmbulance);

        } else {
            redirectAttributes.addFlashAttribute("flashType", "danger");
            redirectAttributes.addFlashAttribute("flashMessage", "commercial.groundAmbulance.edit.success");
            return new ModelAndView("redirect:/groundAmbulance-index.htm");
        }
        return modelAndView;

    }

    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/groundAmbulance-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("groundAmbulance") GroundAmbulance groundAmbulance) {

        LOGGER.info("Runing [ GroundAmbulanceController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            groundAmbulanceService.update(groundAmbulance, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.groundAmbulance.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.groundAmbulance.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/groundAmbulance-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ BusinessUnitController - delete ] id " + id);
        ModelAndView modelAndView = new ModelAndView("redirect:/groundAmbulance-index.htm");
        init(request);
        GroundAmbulance groundAmbulance = groundAmbulanceService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (groundAmbulance != null) {
            try {
                groundAmbulanceService.remove(groundAmbulance, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "commercial.groundAmbulance.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "commercial.groundAmbulance.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("commercial.groundAmbulance.delete.error");
        }
        return Util.getJson(result);

    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }

}
