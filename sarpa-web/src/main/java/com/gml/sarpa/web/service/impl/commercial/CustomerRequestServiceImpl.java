/* * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

 *
 * Document: CustomerRequestServiceImpl.java
 * Created on: Wed Jul 26 17:22:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients requests
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.AirAmbulance;
import com.gml.sarpa.api.entity.commercial.CustomerRequest;
import com.gml.sarpa.api.entity.commercial.GroundAmbulance;
import com.gml.sarpa.api.entity.operation.Journey;
import com.gml.sarpa.api.entity.operation.LoadType;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.commercial.CustomerRequestService;
import com.gml.sarpa.web.repository.commercial.CustomerRequestRepository;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * To manage clients requests
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "ClientRequestService")
@Transactional
public class CustomerRequestServiceImpl implements CustomerRequestService{

    /**
     * Repository for data access
     */
    @Autowired
    private CustomerRequestRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<CustomerRequest> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param clientRequest 
     */
    @Override
    public void save(CustomerRequest  clientRequest ,String userName){
        clientRequest.setVersion(0);
        clientRequest.setUserCreated(userName);
        clientRequest.setDateCreated();
        clientRequest.setUserLastUpdated(userName);
        clientRequest.setDateLastUpdated();
        clientRequest.setCode("SOL-PJ-"+repository.getNextCode());
        
        repository.save(clientRequest );

        auditLogService.auditInsert(clientRequest.getClass().getName(),
                clientRequest.getId(),clientRequest.getVersion(),
                clientRequest.toString(),userName);
    }

    /**
     * Update entity
     * @param customRequest 
     */
    @Override
    public void update(CustomerRequest  customRequest ,String userName){
        CustomerRequest currentClientRequest  = getOne(customRequest.getId());
        customRequest.setVersion(currentClientRequest.getVersion());
        customRequest.setCode(currentClientRequest.getCode());
        customRequest.setUserLastUpdated(userName);
        customRequest.setDateLastUpdated();
        customRequest.setDateCreated(currentClientRequest.getDateCreated());
        customRequest.setUserCreated(currentClientRequest.getUserCreated());
        updateData(currentClientRequest, customRequest);
        repository.save(customRequest );

        if(!currentClientRequest.getCode().equals(customRequest.getCode())){
            auditLogService.auditUpdate(customRequest.getClass().getName(),
            customRequest.getId(),customRequest.getVersion(),
            currentClientRequest.toString(),"customerRequest",customRequest.getCode(),userName);
        }
    }

    /**
     * Delete entity
     *@param clientRequest 
     */
    @Override
    public void remove(CustomerRequest  clientRequest ,String userName){
        Set<Journey> listJourneys = clientRequest.getJourneys(); 
        final boolean isValidateJourneys = listJourneys != null && listJourneys.size() > 0;
        if(isValidateJourneys) {
            listJourneys.clear();
        }
        repository.delete(clientRequest );
        auditLogService.auditDelete(clientRequest.getClass().getName(),
                clientRequest.getId(),clientRequest.getVersion(),
                clientRequest.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public CustomerRequest getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<CustomerRequest> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }
    
    /**
     * Save state objects in pages diferents
     * @param customRequestOld
     * @param customRequestNew 
     */
    private void updateData(CustomerRequest customRequestOld, CustomerRequest customRequestNew) {
        
        
        String probableflightdate = customRequestNew.getProbableflightdate();
        if(probableflightdate != null && probableflightdate.trim().length() > 0) {
            int sizeProbableflightdate = probableflightdate.length();
            int posComma = probableflightdate.lastIndexOf(",");
            if(posComma >= 0) {
                probableflightdate = probableflightdate.substring(posComma + 1,
                    sizeProbableflightdate);
                customRequestNew.setProbableflightdate(probableflightdate);
            }
        }

        //Case 1
        final AirAmbulance configurationOld = customRequestOld.getAirAmbulance();
        final AirAmbulance configurationNew = customRequestNew.getAirAmbulance();
        final boolean isNotEmptyConfigurationNew = configurationNew != null &&
            configurationNew.getId() != null && configurationNew.getId() > 0;
        if(!isNotEmptyConfigurationNew) {
            customRequestNew.setAirAmbulance(configurationOld);
        }
        
        //Case 2
        final Integer numberOfPassengersOld = customRequestOld.getNumberOfPassengers();
        final Integer numberOfPassengersNew = customRequestNew.getNumberOfPassengers();
        final boolean isNotEmptyNumberOfPassengersNew = numberOfPassengersNew != null;
        if(!isNotEmptyNumberOfPassengersNew) {
            customRequestNew.setNumberOfPassengers(numberOfPassengersOld);
        }
        
        final String typeoforganOld = customRequestOld.getTypeoforgan();
        final String typeoforganNew = customRequestNew.getTypeoforgan();
        final boolean isNotEmptyTypeoforganNew = typeoforganNew != null
            && typeoforganNew.trim().length() > 0;
        if(!isNotEmptyTypeoforganNew) {
            customRequestNew.setTypeoforgan(typeoforganOld);
        }
        
        //Case 3
        final String oxygenFiO2Old = customRequestOld.getOxygenFiO2();
        final String oxygenFiO2New = customRequestNew.getOxygenFiO2();
        final boolean isNotEmptyOxygenFiO2New = oxygenFiO2New != null
            && oxygenFiO2New.trim().length() > 0;
        if(!isNotEmptyOxygenFiO2New) {
            customRequestNew.setOxygenFiO2(oxygenFiO2Old);
        }
        
        //Case 4
        final String cityOfServiceOld = customRequestOld.getCityOfService();
        final String cityOfServiceNew = customRequestNew.getCityOfService();
        final boolean isNotEmptyCityOfServiceNew = cityOfServiceNew != null && cityOfServiceNew.trim().length() > 0;
        if(!isNotEmptyCityOfServiceNew) {
            customRequestNew.setCityOfService(cityOfServiceOld);
        }
        
        final String birthPlaceOld = customRequestOld.getBirthPlace();
        final String birthPlaceNew = customRequestNew.getBirthPlace();
        final boolean isNotEmptyBirthPlaceNewNew = birthPlaceNew != null && birthPlaceNew.trim().length() > 0;
        if(!isNotEmptyBirthPlaceNewNew) {
            customRequestNew.setBirthPlace(birthPlaceOld);
        }
        
        final String destinationPlaceOld = customRequestOld.getDestinationPlace();
        final String destinationPlaceNew = customRequestNew.getDestinationPlace();
        final boolean isNotEmptyDestinationPlaceNew = destinationPlaceNew != null && destinationPlaceNew.trim().length() > 0;
        if(!isNotEmptyDestinationPlaceNew) {
            customRequestNew.setDestinationPlace(destinationPlaceOld);
        }
        
        final String detailOld = customRequestOld.getDetail();
        final String detailNew = customRequestNew.getDetail();
        final boolean isNotEmptyetailNew = detailNew != null && detailNew.trim().length() > 0;
        if(!isNotEmptyetailNew) {
            customRequestNew.setDetail(detailOld);
        }
        
        final GroundAmbulance configurationGOld =  customRequestOld.getConfigurationGA();
        final GroundAmbulance configurationGNew =  customRequestNew.getConfigurationGA();
        final boolean isNotEmptyConfigurationGNew = configurationGNew != null 
            && configurationGNew.getId() != null && configurationGNew.getId() > 0;
        if(!isNotEmptyConfigurationGNew) {
            customRequestNew.setConfigurationGA(configurationGOld);
        }
        
        final String typeOld = customRequestOld.getType();
        final String typeNew = customRequestNew.getType();
        final boolean isNotEmptyTypeNew = typeNew != null && typeNew.trim().length() > 0;
        if(!isNotEmptyTypeNew) {
            customRequestNew.setType(typeOld);
        }
        
        //Case 5 and 6
        final String typeOfPassengerOld = customRequestOld.getTypeOfPassenger();
        final String typeOfPassengerNew = customRequestNew.getTypeOfPassenger();
        final boolean isNotEmptyTypeOfPassengerNew = typeOfPassengerNew != null
            && typeOfPassengerNew.trim().length() > 0;
        if(!isNotEmptyTypeOfPassengerNew) {
            customRequestNew.setTypeOfPassenger(typeOfPassengerOld);
        }
        
        //Case 7
        final LoadType loadTypeOld =  customRequestOld.getLoadType();
        final LoadType loadTypeNew =  customRequestNew.getLoadType();
        final boolean isNotEmptyLoadTypeNew = loadTypeNew != null 
            && loadTypeNew.getId() != null && loadTypeNew.getId() > 0;
        if(!isNotEmptyLoadTypeNew) {
            customRequestNew.setLoadType(loadTypeOld);
        }
        
        final String otherLoadTypeOld = customRequestOld.getOtherLoadType();
        final String otherLoadTypeNew = customRequestNew.getOtherLoadType();
        final boolean isNotEmptyOtherLoadTypeNew = otherLoadTypeNew != null
            && otherLoadTypeNew.trim().length() > 0;
        if(!isNotEmptyOtherLoadTypeNew) {
            customRequestNew.setOtherLoadType(otherLoadTypeOld);
        }
        
        final Double loadTypeLongOld = customRequestOld.getLoadTypeLong();
        final Double loadTypeLongNew = customRequestNew.getLoadTypeLong();
        final boolean isNotEmptyLoadTypeLongNew = loadTypeLongNew != null
            && loadTypeLongNew > 0;
        if(!isNotEmptyLoadTypeLongNew) {
            customRequestNew.setLoadTypeLong(loadTypeLongOld);
        }
        
        final Double loadTypeWidthOld = customRequestOld.getLoadTypeWidth();
        final Double loadTypeWidthNew = customRequestNew.getLoadTypeWidth();
        final boolean isNotEmptyLoadTypeWidthNew = loadTypeWidthNew != null
            && loadTypeWidthNew > 0;
        if(!isNotEmptyLoadTypeWidthNew) {
            customRequestNew.setLoadTypeWidth(loadTypeWidthOld);
        }
        
        final Double loadTypeHighOld = customRequestOld.getLoadTypeHigh();
        final Double loadTypeHighNew = customRequestNew.getLoadTypeHigh();
        final boolean isNotEmptyLoadTypeHighNew = loadTypeHighNew != null
            && loadTypeHighNew > 0;
        if(!isNotEmptyLoadTypeHighNew) {
            customRequestNew.setLoadTypeHigh(loadTypeHighOld);
        }
        
        final Double loadTypeWeightPerPieceOld = customRequestOld.getLoadTypeWeightPerPiece();
        final Double loadTypeWeightPerPieceNew = customRequestNew.getLoadTypeWeightPerPiece();
        final boolean isNotEmptyLoadTypeWeightPerPieceNew = loadTypeWeightPerPieceNew != null
            && loadTypeWeightPerPieceNew > 0;
        if(!isNotEmptyLoadTypeWeightPerPieceNew) {
            customRequestNew.setLoadTypeWeightPerPiece(loadTypeWeightPerPieceOld);
        }
    }

    
}
