/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AuditLogController .java
 * Created on: Tue Jul 11 16:47:06 COT 2017
 * Project: sarpa
 * Objective: To audit actions over entityes
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.audit;

import com.gml.sarpa .web.controller.ControllerDefinition;
import com.gml.sarpa .api.entity.auth.Session;
import com.gml.sarpa .api.entity.audit.AuditLog;
import com.gml.sarpa .api.services.audit.AuditLogService;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa .api.utilities.Util.SESSION_USER;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;



/**
 * To audit actions over entityes
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class AuditLogController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(AuditLogController .class);

    @Autowired
    private AuditLogService entityService;


    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/auditLog -index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ AuditLogController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("audit/auditLog/index");
        init(modelAndView,request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/auditLog -create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ AuditLogController - create ]");
        ModelAndView modelAndView = new ModelAndView("audit/auditLog/create");
        init(modelAndView,request);
        
        AuditLog auditLog = new AuditLog();
        modelAndView.addObject("auditLog", auditLog);
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/auditLog -save.htm", method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        @ModelAttribute("auditLog") AuditLog auditLog,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ AuditLogController - save ]");
        ModelAndView modelAndView = new ModelAndView("redirect:/auditLog/index.htm");
        init(modelAndView,request);
       
        try {
            entityService.save(auditLog);
            redirectAttributes.addFlashAttribute("flashType","success");
            redirectAttributes.addFlashAttribute("flashMessage","audit.auditLog .save.success");
        } catch (Exception e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","audit.auditLog .save.error");
        }
        
        return modelAndView;
    }
    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/auditLog -edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ AuditLogController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("audit/auditLog/edit");
        init(modelAndView,request);
        
        AuditLog auditLog = entityService.getOne(id);

        if (auditLog != null){
            modelAndView.addObject("auditLog", auditLog);
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","audit.auditLog .edit.success");
            return new ModelAndView("redirect:/auditLog -index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/auditLog -update.htm", method = RequestMethod.POST)
    public ModelAndView update(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        final RedirectAttributes redirectAttributes,    
        @ModelAttribute("auditLog") AuditLog auditLog) {
        
        LOGGER.info("Runing [ AuditLogController - update ]");
        ModelAndView modelAndView = new ModelAndView("redirect:/auditLog -index.htm");
        init(modelAndView,request);

        try {
            entityService.save(auditLog);
            redirectAttributes.addFlashAttribute("flashType","success");
            redirectAttributes.addFlashAttribute("flashMessage","audit.auditLog .update.success");
        } catch (Exception e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","audit.auditLog .update.error");
        }
        
        return modelAndView;
    }

    private void init(ModelAndView modelAndView,HttpServletRequest request) {
        Session userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
        if (null != userSession && null != userSession.getUser()){
            modelAndView.addObject("userCompleteName",userSession.getUser().getPerson().getCompleteName());
        }    
    }
}
