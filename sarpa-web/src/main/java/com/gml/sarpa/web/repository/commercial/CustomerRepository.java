/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerRepository.java
 * Created on: Tue Jul 25 20:03:42 COT 2017
 * Project: sarpa
 * Objective: To manage curstomers
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage curstomers
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.customer  where name ilike %?1% ", nativeQuery = true)
    public List<Customer> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.customer where id = ?1 ", nativeQuery = true)
    public Customer get(Long id);
    
        /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.customer_sequence') ", nativeQuery = true)
    public Long getNextCode();
    
        /**
     * Search next code value
     */
    @Query(value =
        "select nextval('public.hibernate_sequence') ", nativeQuery = true)
    public Long getNextId();


}
