/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CompanyController.java
 * Created on: Wed Jul 19 16:36:43 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.Company;
import com.gml.sarpa.api.services.commercial.CompanyService;
import com.gml.sarpa.api.services.parametric.TypeOfCompanyRelationshipService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;


/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class CompanyController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(CompanyController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private CompanyService entityService;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private TypeOfCompanyRelationshipService entityServiceTRCompany;


    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/company-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ CompanyController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("commercial/company/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/company-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CompanyController - create ]");
        ModelAndView modelAndView = new ModelAndView("commercial/company/create");
        init(request);
        
        Company company = new Company();
        modelAndView.addObject("company", company);
        modelAndView.addObject("typeOfCompanyRelationshipList", entityServiceTRCompany.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/company-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("company") Company company) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ CompanyController - save ]");
        init(request);
        try {
            entityService.save(company, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.company.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.company.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/company-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ CompanyController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("commercial/company/edit");
        init(request);
        
        Company company = entityService.getOne(id);

        if (company != null){
            modelAndView.addObject("company", company);
            modelAndView.addObject("typeOfCompanyRelationshipList", entityServiceTRCompany.findAll());
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","commercial.company.edit.success");
            return new ModelAndView("redirect:/company-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/company-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("company") Company company) {

        LOGGER.info("Runing [ CompanyController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(company, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.company.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.company.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/company-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ CompanyController - delete ] id "+id);
        init(request);
        
        Company object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "commercial.company.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "commercial.company.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("commercial.company.delete.error");
        }
        return Util.getJson(result);
        
    }
    
    @RequestMapping(value = "/company-findAll.htm", method = RequestMethod.POST)
    @ResponseBody
    public String getFindAll(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("company") Company company) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ CompanyController - getFindAll ]");
        init(request);
        try {
            result.setCode("success");
			result.setData(this.entityService.findAll());
            result.setMessage(messageSource.getMessage(
                    "commercial.company.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.company.save.error", null, locale));
        }
        return Util.getJson(result);
    }
    
    @RequestMapping(value = "/company-findFilter.htm", method = RequestMethod.GET)
    @ResponseBody
    public String getFindFilter(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "name", required = true) String name) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ CompanyController - getFindFilter ]");
        init(request);
        try {
            result.setCode("success");
			result.setData(this.entityService.getAllByFilter(name));
            result.setMessage(messageSource.getMessage(
                    "commercial.company.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.company.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
