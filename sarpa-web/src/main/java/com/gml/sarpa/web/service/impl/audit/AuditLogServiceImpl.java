/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AuditLogServiceImpl .java
 * Created on: Tue Jul 11 16:47:06 COT 2017
 * Project: sarpa
 * Objective: To audit actions over entityes
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.audit;

import com.gml.sarpa .api.entity.audit.AuditLog;
import com.gml.sarpa .api.services.audit.AuditLogService;
import com.gml.sarpa .web.repository.audit.AuditLogRepository;
import org.apache.log4j.Logger;
import java.util.List;
import com.gml.sarpa.commons.util.Util;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * To audit actions over entityes
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "AuditLogService")
public class AuditLogServiceImpl implements AuditLogService{

    /**
     * Repository for data access
     */
    @Autowired
    private AuditLogRepository repository;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<AuditLog> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param auditLog
     */
    @Override
    public void save(AuditLog auditLog){
        repository.save(auditLog);
    }

    /**
     * Update entity
     * @param auditLog
     */
    @Override
    public void update(AuditLog auditLog){
        repository.save(auditLog);
    }

    /**
     * Delete entity
     *@param auditLog
     */
    @Override
    public void remove(AuditLog auditLog){

    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public AuditLog getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<AuditLog> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    @Override
    public void auditInsert(String className,Long objectId,
            Integer version,String data,String userName){
        AuditLog auditLog =  new AuditLog();
        auditLog.setAction("INSERT");
        auditLog.setClassName(className);
        auditLog.setDate(new Date());
        auditLog.setObjectId(objectId);
        auditLog.setObjectVersion(version);
        auditLog.setInitialData(data);
        auditLog.setUsername(userName);
        save(auditLog);
    }

    @Override
    public void auditUpdate(String className,Long objectId,
            Integer version,String data,String property, String value,String userName){
        AuditLog auditLog =  new AuditLog();
        auditLog.setAction("UPDATE");
        auditLog.setClassName(className);
        auditLog.setDate(new Date());
        auditLog.setObjectId(objectId);
        auditLog.setObjectVersion(version);
        auditLog.setInitialData(data);
        auditLog.setProperty(property);
        auditLog.setValue(value);
        auditLog.setUsername(userName);
        save(auditLog);
    }

    @Override
    public void auditDelete(String className,Long objectId,
            Integer version,String data,String userName) {
        AuditLog auditLog =  new AuditLog();
        auditLog.setAction("DELETE");
        auditLog.setClassName(className);
        auditLog.setDate(new Date());
        auditLog.setObjectId(objectId);
        auditLog.setObjectVersion(version);
        auditLog.setInitialData(data);
        auditLog.setUsername(userName);
        save(auditLog);
    }
    
}
