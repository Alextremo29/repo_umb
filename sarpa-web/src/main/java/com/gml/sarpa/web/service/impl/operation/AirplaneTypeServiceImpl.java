/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneTypeServiceImpl .java
 * Created on: Thu Jun 29 11:47:51 COT 2017
 * Project: sarpa
 * Objective: To define AirplaneType Types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.operation;

import com.gml.sarpa.api.entity.operation.AirplaneType;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.operation.AirplaneTypeService;
import com.gml.sarpa.web.repository.operation.AirplaneTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define AirplaneType Types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "AirplaneTypeService")
public class AirplaneTypeServiceImpl implements AirplaneTypeService{

    /**
     * Repository for data access
     */
    @Autowired
    private AirplaneTypeRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<AirplaneType> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param airplaneType
     */
    @Override
    public void save(AirplaneType airplaneType,String userName){
        airplaneType.setVersion(0);
        airplaneType.setUserCreated(userName);
        airplaneType.setDateCreated();
        airplaneType.setUserLastUpdated(userName);
        airplaneType.setDateLastUpdated();
        repository.save(airplaneType);
        auditLogService.auditInsert(airplaneType.getClass().getName(),
                airplaneType.getId(),airplaneType.getVersion(),
                airplaneType.toString(),userName);
    }

    /**
     * Update entity
     * @param airplaneType
     */
    @Override
    public void update(AirplaneType airplaneType,String userName){
        AirplaneType currentAirplaneType = getOne(airplaneType.getId());
        airplaneType.setVersion(currentAirplaneType.getVersion());
        airplaneType.setUserLastUpdated(userName);
        airplaneType.setDateLastUpdated();
        repository.save(airplaneType);
        if(!currentAirplaneType.getName().equals(airplaneType.getName())){
            auditLogService.auditUpdate(airplaneType.getClass().getName(),
            airplaneType.getId(),airplaneType.getVersion(),
            currentAirplaneType.toString(),"name",airplaneType.getName(),userName);
        }
        
    }

    /**
     * Delete entity
     *@param airplaneType
     */
    @Override
    public void remove(AirplaneType airplaneType,String userName){
        repository.delete(airplaneType);
        auditLogService.auditDelete(airplaneType.getClass().getName(),
                airplaneType.getId(),airplaneType.getVersion(),
                airplaneType.toString(),userName);
    }


    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public AirplaneType getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<AirplaneType> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
