/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CompanyRepository.java
 * Created on: Wed Jul 19 16:36:43 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.Company;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CompanyRepository extends JpaRepository<Company, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.company  where name ilike %?1% ", nativeQuery = true)
    public List<Company> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.company where id = ?1 ", nativeQuery = true)
    public Company get(Long id);
    
    /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.company_sequence') ", nativeQuery = true)
    public Long getNextCode();
}
