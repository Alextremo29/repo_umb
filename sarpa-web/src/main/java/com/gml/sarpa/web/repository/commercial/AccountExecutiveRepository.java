/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AccountExecutiveRepository .java
 * Created on: Fri Jun 30 22:08:06 COT 2017
 * Project: sarpa
 * Objective: To define commercial account executives
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.AccountExecutive;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define commercial account executives
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AccountExecutiveRepository extends JpaRepository<AccountExecutive, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.account_executive  where name ilike %?1% ", nativeQuery = true)
    public List<AccountExecutive> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.account_executive where id = ?1 ", nativeQuery = true)
    public AccountExecutive get(Long id);
    
    /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.account_executive_sequence') ", nativeQuery = true)
    public Long getNextCode();
}
