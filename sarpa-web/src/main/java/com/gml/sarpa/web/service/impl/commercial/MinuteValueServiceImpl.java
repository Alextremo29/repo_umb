/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MinuteValueServiceImpl.java
 * Created on: Fri Jul 21 10:43:45 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.MinuteValue;
import com.gml.sarpa.api.services.audit.AuditLogService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gml.sarpa.api.services.commercial.MinuteValueService;
import com.gml.sarpa.api.services.operation.AirplaneTypeService;
import com.gml.sarpa.api.services.operation.ServiceTypeService;
import com.gml.sarpa.web.repository.commercial.MinuteValueRepository;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "MinuteValueService")
public class MinuteValueServiceImpl implements MinuteValueService{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(MinuteValueServiceImpl.class);
    
    /**
     * Repository for data access
     */
    @Autowired
    private MinuteValueRepository repository;
    
    @Autowired
    private AirplaneTypeService airplaneTypeService;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    @Autowired
    private ServiceTypeService serviceTypeService;
    /**
     * List all entities
     * @return
     */
    @Override
    public List<MinuteValue> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param minuteValue 
     */
    @Override
    public void save(MinuteValue  minuteValue ,String userName){
        minuteValue.setVersion(0);
        minuteValue.setUserCreated(userName);
        minuteValue.setDateCreated();
        minuteValue.setUserLastUpdated(userName);
        minuteValue.setDateLastUpdated();
        minuteValue.setAirplaneType(airplaneTypeService.getOne(minuteValue.getAirplaneType().getId()));
        minuteValue.setServiceType(serviceTypeService.getOne(minuteValue.getServiceType().getId()));
        repository.save(minuteValue );
        auditLogService.auditInsert(minuteValue.getClass().getName(),
                minuteValue.getId(),minuteValue.getVersion(),
                minuteValue.toString(),userName);
    }

    /**
     * Update entity
     * @param minuteValue 
     */
    @Override
    public void update(MinuteValue  minuteValue ,String userName){
        MinuteValue  currentMinuteValue  = getOne(minuteValue.getId());
        minuteValue.setVersion(currentMinuteValue.getVersion());
        minuteValue.setUserLastUpdated(userName);
        minuteValue.setDateLastUpdated();
        minuteValue.setDateCreated(currentMinuteValue.getDateCreated());
        minuteValue.setUserCreated(currentMinuteValue.getUserCreated());
        repository.save(minuteValue );
        if(!currentMinuteValue.getValue().equals(minuteValue.getValue())){
            auditLogService.auditUpdate(minuteValue.getClass().getName(),
            minuteValue.getId(),minuteValue.getVersion(),
            currentMinuteValue.toString(),"value",minuteValue.getValue().toString(),userName);
        }
    }

    /**
     * Delete entity
     *@param minuteValue 
     */
    @Override
    public void remove(MinuteValue  minuteValue ,String userName){
        repository.delete(minuteValue );
        auditLogService.auditDelete(minuteValue.getClass().getName(),
                minuteValue.getId(),minuteValue.getVersion(),
                minuteValue.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public MinuteValue getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<MinuteValue> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
