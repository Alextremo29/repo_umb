/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: JourneyRepository.java
 * Created on: Mon Jul 10 16:26:09 COT 2017
 * Project: sarpa
 * Objective: To define journeys times
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.operation;

import com.gml.sarpa.api.entity.operation.Journey;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define journeys times
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface JourneyRepository extends JpaRepository<Journey, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from operation.journey  where name ilike %?1% ", nativeQuery = true)
    public List<Journey> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from operation.journey where id = ?1 ", nativeQuery = true)
    public Journey get(Long id);
    
    @Query(value =
        "select * from operation.journey where "
        + "(0 = ?2 or airplane_type_id = ?1 ) ", nativeQuery = true)
    List<Journey> getJorneysByAirlineType(Long idAirplaneType, Long flagIdAirplaneType);
}
