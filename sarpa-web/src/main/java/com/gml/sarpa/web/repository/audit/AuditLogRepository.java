/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AuditLogRepository .java
 * Created on: Tue Jul 11 16:47:06 COT 2017
 * Project: sarpa
 * Objective: To audit actions over entityes
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.audit;

import com.gml.sarpa .api.entity.audit.AuditLog;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To audit actions over entityes
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AuditLogRepository extends JpaRepository<AuditLog, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from audit.audit_log  where name ilike %?1% ", nativeQuery = true)
    public List<AuditLog> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from audit.audit_log where id = ?1 ", nativeQuery = true)
    public AuditLog get(Long id);
}
