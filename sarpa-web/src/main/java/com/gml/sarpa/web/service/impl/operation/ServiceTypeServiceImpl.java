/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ServiceTypeServiceImpl .java
 * Created on: Fri Jun 30 23:16:43 COT 2017
 * Project: sarpa
 * Objective: To define services types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.operation;

import com.gml.sarpa.api.entity.operation.ServiceType;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.operation.ServiceTypeService;
import com.gml.sarpa.web.repository.operation.ServiceTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define services types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "ServiceTypeService")
public class ServiceTypeServiceImpl implements ServiceTypeService{

    /**
     * Repository for data access
     */
    @Autowired
    private ServiceTypeRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<ServiceType> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param serviceType
     */
    @Override
    public void save(ServiceType serviceType,String userName){
        serviceType.setVersion(0);
        serviceType.setUserCreated(userName);
        serviceType.setDateCreated();
        serviceType.setUserLastUpdated(userName);
        serviceType.setDateLastUpdated();
        repository.save(serviceType);
        auditLogService.auditInsert(serviceType.getClass().getName(),
                serviceType.getId(),serviceType.getVersion(),
                serviceType.toString(),userName);
    }

    /**
     * Update entity
     * @param serviceType
     */
    @Override
    public void update(ServiceType serviceType,String userName){
        ServiceType currentServiceType = getOne(serviceType.getId());
        serviceType.setVersion(currentServiceType.getVersion());
        serviceType.setUserLastUpdated(userName);
        serviceType.setDateLastUpdated();
        repository.save(serviceType);
        if(!currentServiceType.getName().equals(serviceType.getName())){
            auditLogService.auditUpdate(serviceType.getClass().getName(),
            serviceType.getId(),serviceType.getVersion(),
            currentServiceType.toString(),"base",serviceType.getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param serviceType
     */
    @Override
    public void remove(ServiceType serviceType,String userName){
        repository.delete(serviceType);
        auditLogService.auditDelete(serviceType.getClass().getName(),
                serviceType.getId(),serviceType.getVersion(),
                serviceType.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public ServiceType getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<ServiceType> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
