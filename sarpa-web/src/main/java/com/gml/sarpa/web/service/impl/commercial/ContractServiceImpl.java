/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractServiceImpl.java
 * Created on: Fri Jul 21 15:40:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients contracts
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.Contract;
import com.gml.sarpa.api.services.commercial.ContractService;
import com.gml.sarpa.web.repository.commercial.ContractRepository;
import com.gml.sarpa.api.services.audit.AuditLogService;
import org.apache.log4j.Logger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To manage clients contracts
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "ContractService")
public class ContractServiceImpl implements ContractService{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(ContractServiceImpl.class);
    
    /**
     * Repository for data access
     */
    @Autowired
    private ContractRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Contract> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param contract 
     */
    @Override
    public void save(Contract  contract ,String userName){
        contract.setVersion(0);
        contract.setUserCreated(userName);
        contract.setDateCreated();
        contract.setUserLastUpdated(userName);
        contract.setDateLastUpdated();
        contract.setCode("CONT-"+repository.getNextCode());
        repository.save(contract );
        auditLogService.auditInsert(contract.getClass().getName(),
                contract.getId(),contract.getVersion(),
                contract.toString(),userName);
    }

    /**
     * Update entity
     * @param contract 
     */
    @Override
    public void update(Contract  contract ,String userName){
        Contract  currentContract  = getOne(contract.getId());
        contract.setVersion(currentContract.getVersion());
        contract.setDateCreated(currentContract.getDateCreated());
        contract.setUserCreated(currentContract.getUserCreated());
        contract.setUserLastUpdated(userName);
        contract.setDateLastUpdated();
        repository.save(contract );
        
        if(!currentContract.getInitDate().equals(contract.getInitDate())){
            auditLogService.auditUpdate(contract.getClass().getName(),
            contract.getId(),contract.getVersion(),
            currentContract.toString(),"initDate",contract.getInitDate().toString(),userName);
        }
    }

    /**
     * Delete entity
     *@param contract 
     */
    @Override
    public void remove(Contract  contract ,String userName){
        repository.delete(contract );
        auditLogService.auditDelete(contract.getClass().getName(),
                contract.getId(),contract.getVersion(),
                contract.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Contract getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Contract> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
