/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: IndexController.java
 * Created on: 2017/01/06, 12:36:46 PM
 * Project: SARPA
 * Objective: Controller for to define home page actions.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl;

import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.auth.User;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
/**
 * Controller for to define home page actions.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class IndexController implements ControllerDefinition {

    /**
     * Logger de la clase <code>IndexController</code>
     */
    private static final Logger LOGGER = Logger.getLogger(IndexController.class);

    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index.
     *
     * @param request
     * @param response
     * @param session
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        final RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Ejecutando metodo [ onDisplay IndexController ]");
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("user",new User());
        
        try {
           LOGGER.info(locale.toString()); 
        } catch (Exception e) {
            LOGGER.error("Error getting locale: "+e);
        } 
        return modelAndView;
    }
    
    /**
     * M&eacute;todo que se ejecuta al abrir la ventana de index.
     *
     * @param request
     * @param response
     * @param session
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/dashboard.htm", method = RequestMethod.GET)
    public ModelAndView dashboard(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        final RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Ejecutando metodo [ dashboard IndexController ]");
        ModelAndView modelAndView = new ModelAndView("dashboard");
        Session userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
        if (null != userSession && null != userSession.getUser()){
            modelAndView.addObject("userCompleteName",userSession.getUser().getPerson().getCompleteName());
            return modelAndView;
        }
        else{
            return new ModelAndView("redirect:/index.htm");
        }
    }
}
