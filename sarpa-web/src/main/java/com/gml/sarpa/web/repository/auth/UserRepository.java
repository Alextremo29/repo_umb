/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserRepository.java
 * Created on: 2017/02/17, 17:36:46 PM
 * Project: SARPA
 * Objective: Jpa Repository for value list financial entitys.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.auth;

import com.gml.sarpa.api.entity.auth.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Provide jpa functions for value list financial entitys
 *
 * <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Search client by dni
     */
    @Query(value =
        "select * from AUTH.USERS  where CLIENT_ID = ?1 ", nativeQuery = true)
    User findByClient(Long clientId);
   
    /**
     * Search client by username
     */
    @Query(value =
        "select * from AUTH.USERS  where USERNAME = ?1 ", nativeQuery = true)
    User findByUsername(String username);

    /**
     * Search client by username and password
     */
    @Query(value =
        "select * from AUTH.USERS  where UPPER(USERNAME) = UPPER(?1) "+
                "and password = ?2", nativeQuery = true)
    User findByUsernameAndPassword(String username, String password);

    /**
     * Search trade clients
     * @return 
     */
    @Query(value =
        "select * from AUTH.USERS  where TRADE_ID is not null "
            , nativeQuery = true)
    public List<User> getAllByTradeNotNull();
    
    /**
     * Search trade clients
     * @return 
     */
    @Query(value =
        "select * from AUTH.USERS  where CLIENT_ID is not null "
            , nativeQuery = true)
    public List<User> getAllByClientNotNull();
    
    /**
     * Search entity by id
     */
    @Query(value =
        "select * from AUTH.USERS where id = ?1 ", nativeQuery = true)
    public User get(Long id);
    
    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from AUTH.USERS  where name ilike %?1% ", nativeQuery = true)
    public List<User> getAllByFilter(String filter);
}
