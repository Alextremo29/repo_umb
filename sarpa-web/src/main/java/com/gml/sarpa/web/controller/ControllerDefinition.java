/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ControllerDefinition.java
 * Created on: 2016/11/14, 01:32:24 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de
 *                  protecci&oacute;n al cesante.
 * Object: Define controllers behavior.
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller;

import java.io.Serializable;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Define controllers behavior.
 *
 * @author <a href="mailto:manuelm@gmlsoftware.com">Manuel Mart&iacute;z</a>
 */
public interface ControllerDefinition extends Serializable {

    /**
     * M&eacute;todo que se inicia al invocar la vista.
     *
     * @param request
     * @param response
     * @param locale
     *
     * @return
     */
    ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response,Locale locale,HttpSession session,
        final RedirectAttributes redirectAttributes);
}
