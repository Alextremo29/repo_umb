/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactController.java
 * Created on: Fri Jun 30 22:30:20 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.people;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.people.Contact;
import com.gml.sarpa.api.services.people.ContactService;
import com.gml.sarpa.api.services.people.ContactTypeService;
import com.gml.sarpa.api.services.people.PersonService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class ContactController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(ContactController.class);

    /**
     * Session user information
     */
    private Session userSession;
    
    @Autowired
    private ContactService entityService;

    @Autowired
    private PersonService personService;
    
    @Autowired
    private ContactTypeService contactTypeService;
    
    @Autowired
    private MessageSource messageSource;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/contact-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ ContactController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("people/contact/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/contact-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ ContactController - create ]");
        ModelAndView modelAndView = new ModelAndView("people/contact/create");
        init(request);
        
        Contact contact = new Contact();
        modelAndView.addObject("contact", contact);
        modelAndView.addObject("personList", personService.findAll());
        modelAndView.addObject("contactTypeList", contactTypeService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/contact-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("contact") Contact contact) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ ContactController - save ]");
        init(request);
        try {
            entityService.save(contact, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "people.contact.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.contact.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/contact-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ ContactController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("people/contact/edit");
        init(request);
        
        Contact contact = entityService.getOne(id);

        if (contact != null){
            modelAndView.addObject("contact", contact);
            modelAndView.addObject("personList", personService.findAll());
            modelAndView.addObject("contactTypeList", contactTypeService.findAll());
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","people.contact.edit.success");
            return new ModelAndView("redirect:/contact-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/contact-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("contact") Contact contact) {

        LOGGER.info("Runing [ ContactController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(contact, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "people.contact.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.contact.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    
    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/contact-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ ContactController - delete ] id "+id);
        init(request);
        
        Contact object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "people.contact.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "people.contact.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("people.contact.delete.error");
        }
        return Util.getJson(result);
        
    }
    /**
     * 
     * @param model
     * @param request
     * @param locale
     * @param value
     * @return 
     */
    @RequestMapping(value = "/contact-findContactsParams.htm", method = RequestMethod.GET)
    @ResponseBody
    public String getContactsParams(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "value", required = true) String value) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Running [ ContactController - getContactsParams ]");
        init(request);
        try {
            result.setCode("success");
            final Long fillValue = value != null && value.trim().length() > 0 ? 1L : 0L;
            result.setData(this.entityService.getContactsParams(value, fillValue));
            result.setMessage(messageSource.getMessage(
                    "people.contact.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.contact.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
