/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeePositionController.java
 * Created on: Wed Jul 19 12:45:39 COT 2017
 * Project: sarpa
 * Objective: To manage employees positions
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.parametric;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.web.controller.ControllerDefinition;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.parametric.EmployeePosition;
import com.gml.sarpa.api.services.parametric.EmployeePositionService;
import com.gml.sarpa.api.utilities.Util;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * To manage employees positions
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class EmployeePositionController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(EmployeePositionController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private EmployeePositionService entityService;

    @Autowired
    private MessageSource messageSource;
    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/employeePosition-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ EmployeePositionController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("parametric/employeePosition/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/employeePosition-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ EmployeePositionController - create ]");
        ModelAndView modelAndView = new ModelAndView("parametric/employeePosition/create");
        init(request);
        
        EmployeePosition employeePosition = new EmployeePosition();
        modelAndView.addObject("employeePosition", employeePosition);
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/employeePosition-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("employeePosition") EmployeePosition employeePosition) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ EmployeePositionController - save ]");
        init(request);
        try {
            entityService.save(employeePosition, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.employeePosition.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.employeePosition.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/employeePosition-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ EmployeePositionController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("parametric/employeePosition/edit");
        init(request);
        
        EmployeePosition employeePosition = entityService.getOne(id);

        if (employeePosition != null){
            modelAndView.addObject("employeePosition", employeePosition);
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","parametric.employeePosition.edit.success");
            return new ModelAndView("redirect:/employeePosition-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/employeePosition-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("employeePosition") EmployeePosition employeePosition) {

        LOGGER.info("Runing [ EmployeePositionController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(employeePosition, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.employeePosition.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.employeePosition.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/employeePosition-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ EmployeePositionController - delete ] id "+id);
        init(request);
        
        EmployeePosition object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "parametric.employeePosition.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "parametric.employeePosition.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("parametric.employeePosition.delete.error");
        }
        return Util.getJson(result);
        
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
