/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportController .java
 * Created on: Wed Jun 28 07:47:20 COT 2017
 * Project: sarpa
 * Objective: To define airports
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.operation;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.operation.Airport;
import com.gml.sarpa.api.services.operation.AirportService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;


/**
 * To define airports
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class AirportController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(AirportController .class);

    @Autowired
    private AirportService entityService;

    /**
     * Session user information
     */
    private Session userSession;
    
    @Autowired
    private MessageSource messageSource;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/airport-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ AirportController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("operation/airport/index");
        init(request);
         
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAllByFlgDestination(true));

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "airport-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ AirportController - create ]");
        ModelAndView modelAndView = new ModelAndView("operation/airport/create");
        init(request);
        
        Airport airport = new Airport();
        modelAndView.addObject("airport", airport);
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/airport-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("airport") Airport airport) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ AirportController - save ]");
        init(request);
        try {
            entityService.save(airport, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "operation.airport.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "operation.airport.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/airport-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ AirportController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("operation/airport/edit");
        init(request);
        
        Airport airport = entityService.getOne(id);

        if (airport != null){
            modelAndView.addObject("airport", airport);
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","operation.airport.edit.error");
            return new ModelAndView("redirect:/airport-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/airport-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("airport") Airport airport) {

        LOGGER.info("Runing [ AirportController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(airport, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "operation.airport.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "operation.airport.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    
    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/airport-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ AirportController - delete ] id "+id);
        init(request);
        
        Airport object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "operation.airport.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "operation.airport.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("operation.airport.delete.error");
        }
        return Util.getJson(result);
        
    }
    
    /**
     *
     * @param model
     * @param request
     * @param locale
     * @param name
     * @return
     */
    @RequestMapping(value = "/airport-FilterByNameOrIATACodeOrCity.htm", method =
        RequestMethod.GET)
    @ResponseBody
    public String getFilterByNameOrIATACodeOrCity(Model model, HttpServletRequest request,
        Locale locale,
        @RequestParam(value = "name", required = true) String name) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ AirportController - getFilterByNameOrIATACodeOrCity ]");
        init(request);
        try {
            result.setCode("success");
            final Long fillName = name != null && name.trim().length() > 0 ? 1L : 0L;
            result.setData(this.entityService.getAirportsByCityOrNameOrIataCode(name, fillName));
            result.setMessage(messageSource.getMessage(
                "operation.airport.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                "operation.airport.save.error", null, locale));
        }
        return Util.getJson(result);
    }
    
    @RequestMapping(value = "/airport-findCitiesInAirport.htm", method = RequestMethod.GET)
    @ResponseBody
    public String getCitiesInAirport(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "city", required = true) String city) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Running [ AirportController - getCitiesInAirport ]");
        init(request);
        try {
            result.setCode("success");
			result.setData(this.entityService.getCitiesInAirport(city));
            result.setMessage(messageSource.getMessage(
                    "operation.airport.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "operation.airport.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
