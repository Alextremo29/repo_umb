/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DelayCauseServiceImpl .java
 * Created on: Mon Jul 17 06:41:33 COT 2017
 * Project: sarpa
 * Objective: To manage Grounds for take-off delays
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa .api.entity.parametric.DelayCause;
import com.gml.sarpa .api.services.parametric.DelayCauseService;
import com.gml.sarpa .web.repository.parametric.DelayCauseRepository;
import com.gml.sarpa .api.services.audit.AuditLogService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To manage Grounds for take-off delays
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "DelayCauseService")
public class DelayCauseServiceImpl implements DelayCauseService{

    /**
     * Repository for data access
     */
    @Autowired
    private DelayCauseRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<DelayCause> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param delayCause 
     */
    @Override
    public void save(DelayCause  delayCause ,String userName){
        delayCause.setVersion(0);
        delayCause.setUserCreated(userName);
        delayCause.setDateCreated();
        delayCause.setUserLastUpdated(userName);
        delayCause.setDateLastUpdated();
        repository.save(delayCause );
        auditLogService.auditInsert(delayCause.getClass().getName(),
                delayCause.getId(),delayCause.getVersion(),
                delayCause.toString(),userName);
    }

    /**
     * Update entity
     * @param delayCause 
     */
    @Override
    public void update(DelayCause  delayCause ,String userName){
        DelayCause  currentDelayCause  = getOne(delayCause.getId());
        delayCause.setVersion(currentDelayCause .getVersion());
        delayCause.setUserLastUpdated(userName);
        delayCause.setDateLastUpdated();
        delayCause.setDateCreated(currentDelayCause.getDateCreated());
        delayCause.setUserCreated(currentDelayCause.getUserCreated());
        repository.save(delayCause );
        if(!currentDelayCause .getName().equals(delayCause.getName())){
            auditLogService.auditUpdate(delayCause.getClass().getName(),
            delayCause.getId(),delayCause.getVersion(),
            currentDelayCause .toString(),"name",delayCause.getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param delayCause 
     */
    @Override
    public void remove(DelayCause  delayCause ,String userName){
        repository.delete(delayCause );
        auditLogService.auditDelete(delayCause.getClass().getName(),
                delayCause.getId(),delayCause.getVersion(),
                delayCause.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public DelayCause getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<DelayCause> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
