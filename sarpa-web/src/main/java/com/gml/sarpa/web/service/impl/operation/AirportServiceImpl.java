/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportServiceImpl .java
 * Created on: Wed Jun 28 07:47:20 COT 2017
 * Project: sarpa
 * Objective: To define airports
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.operation;

import com.gml.sarpa.api.entity.operation.Airport;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.operation.AirportService;
import com.gml.sarpa.web.repository.operation.AirportRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define airports
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "AirportService")
public class AirportServiceImpl implements AirportService{

    /**
     * Repository for data access
     */
    @Autowired
    private AirportRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Airport> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param airport
     */
    @Override
    public void save(Airport airport,String userName){
        airport.setVersion(0);
        airport.setUserCreated(userName);
        airport.setDateCreated();
        airport.setUserLastUpdated(userName);
        airport.setDateLastUpdated();
        repository.save(airport);
        auditLogService.auditInsert(airport.getClass().getName(),
                airport.getId(),airport.getVersion(),
                airport.toString(),userName);
    }

    /**
     * Update entity
     * @param airport
     */
    @Override
    public void update(Airport airport,String userName){
        Airport currentAirport = getOne(airport.getId());
        airport.setVersion(currentAirport.getVersion());
        airport.setUserLastUpdated(userName);
        airport.setDateLastUpdated();
        repository.save(airport);
        if(!currentAirport.getName().equals(airport.getName())){
            auditLogService.auditUpdate(airport.getClass().getName(),
            airport.getId(),airport.getVersion(),
            currentAirport.toString(),"name",airport.getName(),userName);
        }
        
    }

    /**
     * Delete entity
     *@param airport
     */
    @Override
    public void remove(Airport airport,String userName){
        repository.delete(airport);
        auditLogService.auditDelete(airport.getClass().getName(),
                airport.getId(),airport.getVersion(),
                airport.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Airport getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Airport> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    @Override
    public List<Airport> findAllByFlgDestination(boolean flag) {
        return repository.getAllByFlgDestination(flag);
    }

    @Override
    public List<Airport> findAllByFlgBase(boolean flag) {
        return repository.getAllByFlgBase(flag);
    }
    
    @Override
    public List<String> getCitiesInAirport(String city){
        return repository.getCitiesInAirport(city);
    }
    
    @Override
    public List<Airport> getAirportsByCityOrNameOrIataCode(String valueAirport,
        Long fillValueAirport) {
        List<Airport> resultList =  repository.getAirportsByCityOrNameOrIataCode(valueAirport,
            fillValueAirport);
        return resultList;
    }

    
}
