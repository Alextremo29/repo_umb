jQuery(function ($) {
    
    function menuHandler( event ) {
        event.preventDefault();
        $( "#divMainContent" ).load( $(this).attr("href") );
      }
      
    $( "a.menu-item" ).on( "click", {}, menuHandler );
    
});

