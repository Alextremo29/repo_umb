/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: eventsCompany
 * Created on: Thu Oct 10 02:47:32 COT 2017
 * Project: sarpa
 * Objective: To manage events companies.
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
$(function () {

    $('#idNameContact').keyup(function () {
        var valueFind = $(this).val();
        $('#contacts').hide();

        $.ajax({
            type: "GET",
            url: "/sarpa/contact-findContactsParams.htm?value=" + valueFind,
            dataType: "json",
            success: function (response) {
                var listContacts = "";
                var tamanio = response.data.length;
                for (var i = 0; i < tamanio; i++) {
                    var firstName = response.data[i].person.firstName === undefined ? "" : response.data[i].person.firstName;
                    var lastName = response.data[i].person.lastName === undefined ? "" : response.data[i].person.lastName;
                    var fullName = firstName + " " + lastName;
                    var valueContact = response.data[i].value;
                    var contactType = response.data[i].contactType.name;
                    var valueEnd = fullName + "," + contactType + ", " + valueContact;
                    listContacts += "<a class = 'eventContact' data='" + fullName + "' id=" + response.data[i].id + ">" + valueEnd + "</a><br>";
                }
                $('#contacts').fadeIn(1000).html(listContacts);
                //Al hacer click en alguna de las empresas
                $('.eventContact').on('click', function () {
                    var value = $(this).attr('data');
                    var id = $(this).attr('id');
                    $('#idNameContact').val(value);
                    $('#contactHdn').val(id);
                    $('#contacts').fadeOut(1000);
                    $('#contacts').empty();
                });
            },
            error: function (jqXHR, status, error) {
                alert('Invocacion error consulta contactos asociados a \n\
            una empresa- modo Autocompletar -> Consulte con el Administrador del Sistema. ' +
                        'status: ' + status + 'error: ' + error +
                        'jqXHR' + jqXHR);
            }
        });
    });
});
