<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                name : {
                    required: true
                },
                airplaneType : {
                    required: true
                },
                passengersNumber : {
                    required: true
                },
                stretchersNumber : {
                    required: true
                },
                businessUnit : {
                    required: true
                },
                loadingCapacity : {
                    required: true
                },
                grossWeight : {
                    required: true
                }
            },
            messages: {
                name : {
                    required: '<spring:message code="operation.airplane.name.errors.required" />'
                },
                airplaneType : {
                    required: '<spring:message code="operation.airplane.airplaneType.errors.required" />'
                },
                passengersNumber : {
                    required: '<spring:message code="operation.airplane.passengersNumber.errors.required" />'
                },
                stretchersNumber : {
                    required: '<spring:message code="operation.airplane.stretchersNumber.errors.required" />'
                },
                businessUnit : {
                    required: '<spring:message code="operation.airplane.businessUnit.errors.required" />'
                },
                loadingCapacity : {
                    required: '<spring:message code="operation.airplane.loadingCapacity.errors.required" />'
                },
                grossWeight : {
                    required: '<spring:message code="operation.airplane.grossWeight.errors.required" />'
                }
            }
        });
    });
</script>