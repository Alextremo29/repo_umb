<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                { field: "base", headerText: "<spring:message code="operation.journey.base.label" />", width: 105 },
                { field: "destination", headerText: "<spring:message code="operation.journey.destination.label" />", width: 105 },
                { field: "airplaneType", headerText: "<spring:message code="operation.journey.airplaneType.label" />", width: 105 },
                { field: "time", headerText: "<spring:message code="operation.journey.time.label" />", width: 105 },
                { field: "flgInternational", headerText: "<spring:message code="operation.journey.flgInternational.label" />", width: 105 },
                { field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right }
            ]
        });
        
    });
</script>