<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.operation.airport' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="airport-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.create' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> <spring:message code="operation.airport.create.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.create" /></p>
                            </div>

                            <form:form class="form" method="post" action="airport-save.htm" modelAttribute="airport" id="createFrm">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.name.label" /></label>
                                                <form:input class="form-control"  id="nameTxt" path="name"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.city.label" /></label>
                                                <form:input class="form-control"  id="cityTxt" path="city"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.country.label" /></label>
                                                <form:input class="form-control"  id="countryTxt" path="country"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.iataCode.label" /></label>
                                                <form:input class="form-control"  id="iataCodeTxt" path="iataCode"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.icaoCode.label" /></label>
                                                <form:input class="form-control"  id="icaoCodeTxt" path="icaoCode"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.latitude.label" /></label>
                                                <form:input class="form-control"  id="latitudeTxt" path="latitude"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.longitude.label" /></label>
                                                <form:input class="form-control"  id="longitudeTxt" path="longitude"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.altitude.label" /></label>
                                                <form:input class="form-control"  id="altitudeTxt" path="altitude"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.timeZone.label" /></label>
                                                <form:input class="form-control"  id="timeZoneTxt" path="timeZone"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.dst.label" /></label>
                                                <form:input class="form-control"  id="dstTxt" path="dst"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.tzTimezone.label" /></label>
                                                <form:input class="form-control"  id="tzTimezoneTxt" path="tzTimezone"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.flgBase.label" /></label>
                                                <form:input class="form-control"  id="flgBaseTxt" path="flgBase"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.flgDestination.label" /></label>
                                                <form:input class="form-control"  id="flgDestinationTxt" path="flgDestination"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.owner.label" /></label>
                                                <form:input class="form-control"  id="ownerTxt" path="owner"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.operator.label" /></label>
                                                <form:input class="form-control"  id="operatorTxt" path="operator"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.trackLength.label" /></label>
                                                <form:input class="form-control"  id="trackLengthTxt" path="trackLength"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.trackWidth.label" /></label>
                                                <form:input class="form-control"  id="trackWidthTxt" path="trackWidth"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.operationSchedule.label" /></label>
                                                <form:input class="form-control"  id="operationScheduleTxt" path="operationSchedule"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.flgNightOperation.label" /></label>
                                                <form:input class="form-control"  id="flgNightOperationTxt" path="flgNightOperation"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.flgRequiresPermission.label" /></label>
                                                <form:input class="form-control"  id="flgRequiresPermissionTxt" path="flgRequiresPermission"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"  ><spring:message code="operation.airport.permissionDetail.label" /></label>
                                                <form:input class="form-control"  id="permissionDetailTxt" path="permissionDetail"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="airport-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                    <a href="airport-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>

        </div>   
        <%@ include file="/WEB-INF/views/operation/airport/editJsFunctions.jsp" %>    
    </body>
</html>    
