<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#editFrm").validate({
            rules: {
                name : {
                    required: true
                },
                description : {
                    required: true
                },
                detail : {
                    required: true
                },
                autonomy : {
                    required: true
                },
                maxDistance : {
                    required: true
                },
                currency : {
                    required: true
                }
            },
            messages: {
                name : {
                    required: '<spring:message code="operation.airplaneType.name.errors.required" />'
                },
                description : {
                    required: '<spring:message code="operation.airplaneType.description.errors.required" />'
                },
                detail : {
                    required: '<spring:message code="operation.airplaneType.detail.errors.required" />'
                },
                autonomy : {
                    required: '<spring:message code="operation.airplaneType.autonomy.errors.required" />'
                },
                maxDistance : {
                    required: '<spring:message code="operation.airplaneType.maxDistance.errors.required" />'
                },
                currency : {
                    required: '<spring:message code="operation.airplaneType.currency.errors.required" />'
                }
            }
        });
    });
</script>