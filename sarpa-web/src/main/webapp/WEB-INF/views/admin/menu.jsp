<div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class=" nav-item"><a href="index.html"><i class="icon-wrench"></i><span data-i18n="" class="menu-title"><spring:message code='menu.parametric' /></span></a>
                <ul class="menu-content">
                    <li class="menu-item active"><a href="businessUnit-index.htm" id="menuLnk-businessUnit" class="menu-item"><spring:message code='menu.parametric.businessUnit' /></a></li>
                    <li><a href="delayCause-index.htm" id="menuLnk-delayCause" class="menu-item"><spring:message code='menu.parametric.delayCause' /></a></li>
                    <li><a href="employeePosition-index.htm" id="menuLnk-employeePosition" class="menu-item"><spring:message code='menu.parametric.employeePosition' /></a></li>
                    <li><a href="currency-index.htm" id="menuLnk-currency" class="menu-item"><spring:message code='menu.parametric.currency' /></a></li>
                    <li><a href="fbocost-index.htm" id="menuLnk-fbocost" class="menu-item"><spring:message code='menu.parametric.fbocost' /></a></li>
					<li><a href="airportrate-index.htm" id="menuLnk-airportrate" class="menu-item"><spring:message code='menu.parametric.airportrate' /></a></li>
					<li><a href="airportextensionhour-index.htm" id="menuLnk-airportextensionhour" class="menu-item"><spring:message code='menu.parametric.airportextensionhour' /></a></li>
					<li><a href="typeofcompanyrelationship-index.htm" id="menuLnk-typeofcompanyrelationship" class="menu-item"><spring:message code='menu.parametric.typeofcompanyrelationship' /></a></li>
                </ul>
            </li>
            <li class=" nav-item"><a href="index.html"><i class="icon-settings"></i><span data-i18n="" class="menu-title"><spring:message code='menu.operation' /></span></a>
                <ul class="menu-content">
                    <li class="menu-item" ><a href="airport-index.htm" id="menuLnk-airport" class="menu-item" ><spring:message code='menu.operation.airport' /></a></li>
                    <li><a href="airplaneType-index.htm" id="menuLnk-airplaneType" class="menu-item"><spring:message code='menu.operation.airplaneType' /></a></li>
                    <li><a href="airplane-index.htm" id="menuLnk-airplane" class="menu-item"><spring:message code='menu.operation.airplane' /></a></li>
                    <li><a href="loadType-index.htm" id="menuLnk-loadType" class="menu-item"><spring:message code='menu.operation.loadType' /></a></li>
                    <li><a href="serviceType-index.htm" id="menuLnk-serviceType" class="menu-item"><spring:message code='menu.operation.serviceType' /></a></li>
                    <li><a href="journey-index.htm" id="menuLnk-journey" class="menu-item"><spring:message code='menu.operation.journey' /></a></li>
                </ul>
            </li>
            
            <li class=" nav-item"><a href="index.html"><i class="icon-users"></i><span data-i18n="" class="menu-title"><spring:message code='menu.people' /></span></a>
                <ul class="menu-content">
                    <li class="menu-item"><a href="person-index.htm" id="menuLnk-person" class="menu-item"><spring:message code='menu.people.person' /></a></li>
                    <li><a href="contact-index.htm" id="menuLnk-contact" class="menu-item"><spring:message code='menu.people.contact' /></a> </li>
                    <li><a href="employee-index.htm" id="menuLnk-employee" class="menu-item"><spring:message code='menu.people.employee' /></a> </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="index.html"><i class="icon-book-open"></i><span data-i18n="" class="menu-title"><spring:message code='menu.auth' /></span></a>
                <ul class="menu-content">
                    <li class="menu-item"><a href="user-index.htm" id="menuLnk-user" class="menu-item"><spring:message code='menu.auth.user' /></a></li>
                </ul>
            </li>

            <li class=" nav-item"><a href="index.html"><i class="icon-user"></i><span data-i18n="" class="menu-title"><spring:message code='menu.commercial' /></span></a>
                <ul class="menu-content">
                    <li class="menu-item"><a href="accountExecutive-index.htm" id="menuLnk-accountExecutive" class="menu-item"><spring:message code='menu.commercial.accountExecutive' /></a></li>
                    <li><a href="company-index.htm" id="menuLnk-company" class="menu-item"><spring:message code='menu.commercial.company' /></a></li>
					<li><a href="airAmbulance-index.htm" id="menuLnk-airAmbulance" class="menu-item"><spring:message code='menu.commercial.airAmbulance' /></a></li>
                    <li><a href="groundAmbulance-index.htm" id="menuLnk-groundAmbulance" class="menu-item"><spring:message code='menu.commercial.groundAmbulance' /></a></li>
                    <li><a href="customer-index.htm" id="menuLnk-customer" class="menu-item"><spring:message code='menu.commercial.customer' /></a></li>
                    <li><a href="minuteValue-index.htm" id="menuLnk-minuteValue" class="menu-item"><spring:message code='menu.commercial.minuteValue' /></a></li>
                    <li><a href="contractType-index.htm" id="menuLnk-contractValue" class="menu-item"><spring:message code='menu.commercial.contractType' /></a></li>
                    <li><a href="contract-index.htm" id="menuLnk-contract" class="menu-item"><spring:message code='menu.commercial.contract' /></a></li>
                    <li><a href="customerRequest-index.htm" id="menuLnk-customerRequest" class="menu-item"><spring:message code='menu.commercial.customerRequest' /></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
