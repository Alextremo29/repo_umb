<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                code : {
                    required: true
                },
                name : {
                    required: true
                }
            },
            messages: {
                code : {
                    required: '<spring:message code="parametric.delayCause.code.errors.required" />'
                },
                name : {
                    required: '<spring:message code="parametric.delayCause.name.errors.required" />'
                }
                
            }
        });
    });
</script>