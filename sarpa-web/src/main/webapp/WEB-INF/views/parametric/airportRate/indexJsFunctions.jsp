<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true,
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                {field: "city", headerText: '<spring:message code="parametric.airportrate.city.label" />', width: 105},
                {field: "airport", headerText: '<spring:message code="parametric.airportrate.airportgrid.label" />', width: 105},
                {field: "iataCode", headerText: '<spring:message code="parametric.airportrate.iatacode.label" />', width: 105},
                {field: "year", headerText: '<spring:message code="parametric.airportrate.year.label" />', width: 105},
                {field: "currencyType", headerText: '<spring:message code="parametric.airportrate.currencytype.label" />', width: 105},
                {field: "value", headerText: '<spring:message code="parametric.airportrate.value.label" />', width: 105},
                {field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right}
            ]
        });

    });
</script>