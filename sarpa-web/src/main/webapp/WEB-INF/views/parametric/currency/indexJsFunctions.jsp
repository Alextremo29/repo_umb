<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true,
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                {field: "code", headerText: '<spring:message code="parametric.currency.code.label" />', width: 105},
                {field: "name", headerText: '<spring:message code="parametric.currency.name.label" />', width: 105},
                {field: "details", headerText: '<spring:message code="parametric.currency.details.label" />', width: 105},
                {field: "convFactorCopExt", headerText: '<spring:message code="parametric.currency.convFactorCopExt.label" />', width: 105},
                {field: "convFactorExtCop", headerText: '<spring:message code="parametric.currency.convFactorExtCop.label" />', width: 105},
                {field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right}
            ]
        });

    });
</script>