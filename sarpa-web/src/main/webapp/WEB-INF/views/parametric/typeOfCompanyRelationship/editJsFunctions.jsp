<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        $("#editFrm").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '<spring:message code="parametric.typeofcompanyrelationship.name.errors.required" />'
                }
            }
        });
    });
</script>