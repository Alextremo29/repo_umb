<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<form:form method="post" action="logout.htm"> 
    <spring:message code="common.header.welcome" /> ${userCompleteName}
    <button id="logOutBtn" type="submit" class="btn btn-warning btn-xs">
        
        <spring:message code="common.header.signOut" />
    </button>
</form:form>

<div class="row">
    <%@ include file="/WEB-INF/views/admin/menu.jsp" %>
</div>

 <%@ include file="/WEB-INF/views/common/flashMessage.jsp" %>    