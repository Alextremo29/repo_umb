<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true,
            pageSettings: {pageSize: 50},
            allowSearching: true,
            toolbarSettings: {showToolbar: true, toolbarItems: ["search"]},
            columns: [
                {field: "firstName", headerText: "<spring:message code="people.person.firstName.label" />", width: 105},
                {field: "lastName", headerText: "<spring:message code="people.person.lastName.label" />", width: 105},
                {field: "documentNumber", headerText: "<spring:message code="people.person.documentNumber.label" />", width: 105},
                {field: "documentType", headerText: "<spring:message code="people.person.documentType.label" />", width: 105},
                {field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right}
            ]
        });

    });
</script>