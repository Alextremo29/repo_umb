<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                { field: "person", headerText: '<spring:message code="people.employee.person.label" />', width: 105 },
                { field: "type", headerText: '<spring:message code="people.employee.type.label" />', width: 105 },
                { field: "position", headerText: '<spring:message code="people.employee.position.label" />', width: 105 },
                { field: "operativeCode", headerText: '<spring:message code="people.employee.operativeCode.label" />', width: 105 },
                { field: "contractType", headerText: '<spring:message code="people.employee.contractType.label" />', width: 105 },
                { field: "adminStatus", headerText: '<spring:message code="people.employee.adminStatus.label" />', width: 105 },
                { field: "flyStatus", headerText: '<spring:message code="people.employee.flyStatus.label" />', width: 105 },
                { field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right }     
            ]
        });
        
    });
</script>