<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                person: {
                    required: true
                },
                airplaneType: {
                    required: true
                },
                type: {
                    required: true
                },
                position: {
                    required: true
                },
                birthDay: {
                    required: true
                },
                operativeCode: {
                    required: true
                },
                contractType: {
                    required: true
                },
                salary: {
                    required: true
                },
                anualBonus: {
                    required: true
                },
                admissionDate: {
                    required: true
                },
                retirementDate: {
                    required: true
                },
                rh: {
                    required: true
                },
                medCertificatesExpiration: {
                    required: true
                },
                licenseNumber: {
                    required: true
                },
                passportNumber: {
                    required: true
                },
                passportExpiration: {
                    required: true
                },
                americanVisaNumber: {
                    required: true
                },
                americanVisaExpiration: {
                    required: true
                },
                adminStatus: {
                    required: true
                }
            },
            messages: {
                person: {
                    required: '<spring:message code="people.employee.person.errors.required" />'
                },
                airplaneType: {
                    required: '<spring:message code="people.employee.airplaneType.errors.required" />'
                },
                type: {
                    required: '<spring:message code="people.employee.type.errors.required" />'
                },
                position: {
                    required: '<spring:message code="people.employee.position.errors.required" />'
                },
                birthDay: {
                    required: '<spring:message code="people.employee.birthDay.errors.required" />'
                },
                operativeCode: {
                    required: '<spring:message code="people.employee.operativeCode.errors.required" />'
                },
                contractType: {
                    required: '<spring:message code="people.employee.contractType.errors.required" />'
                },
                salary: {
                    required: '<spring:message code="people.employee.salary.errors.required" />'
                },
                anualBonus: {
                    required: '<spring:message code="people.employee.anualBonus.errors.required" />'
                },
                admissionDate: {
                    required: '<spring:message code="people.employee.admissionDate.errors.required" />'
                },
                retirementDate: {
                    required: '<spring:message code="people.employee.retirementDate.errors.required" />'
                },
                rh: {
                    required: '<spring:message code="people.employee.rh.errors.required" />'
                },
                medCertificatesExpiration: {
                    required: '<spring:message code="people.employee.medCertificatesExpiration.errors.required" />'
                },
                licenseNumber: {
                    required: '<spring:message code="people.employee.licenseNumber.errors.required" />'
                },
                passportNumber: {
                    required: '<spring:message code="people.employee.passportNumber.errors.required" />'
                },
                passportExpiration: {
                    required: '<spring:message code="people.employee.passportExpiration.errors.required" />'
                },
                americanVisaNumber: {
                    required: '<spring:message code="people.employee.americanVisaNumber.errors.required" />'
                },
                americanVisaExpiration: {
                    required: '<spring:message code="people.employee.americanVisaExpiration.errors.required" />'
                },
                status: {
                    required: '<spring:message code="people.employee.adminStatus.errors.required" />'
                }
            }
        });
    });
</script>