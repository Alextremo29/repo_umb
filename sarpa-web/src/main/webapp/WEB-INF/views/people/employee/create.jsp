<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.people.employee' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="employee-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.create' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> <spring:message code="people.employee.create.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.create" /></p>
                            </div>

                            <form:form class="form" method="post" action="employee-save.htm" modelAttribute="employee" id="createFrm">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.person.label" /></label>
                                                <form:select class="form-control"  id="personId" items="${personList}" path="person.id" itemLabel="completeName" itemValue="id"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.airplaneType.label" /></label>
                                                <form:select class="form-control"  id="airplaneTypeId" items="${airplaneTypeList}" path="airplaneType.id" itemLabel="name" itemValue="id"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.type.label" /></label>
                                                <form:select class="form-control"  id="typeSel"  path="type">
                                                    <option <c:if test="${'land'.equals(employee.type)}">selected="true"</c:if> value="land"><spring:message code="people.employee.type.land" /></option>
                                                    <option <c:if test="${'aerial'.equals(employee.type)}">selected="true"</c:if> value="aerial"><spring:message code="people.employee.type.aerial" /></option>
                                                    <option <c:if test="${'aerial-medical'.equals(employee.type)}">selected="true"</c:if> value="aerial-medical"><spring:message code="people.employee.type.aerial-medical" /></option>
                                                </form:select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.position.label" /></label>
                                                <form:select class="form-control"  id="employeePositionId" items="${employeePositionList}" path="position.id" itemLabel="name" itemValue="id"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.birthDay.label" /></label>
                                                <form:input class="form-control"  id="birthDayTxt" path="birthDay"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.operativeCode.label" /></label>
                                                <form:input class="form-control"  id="operativeCodeTxt" path="operativeCode"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.contractType.label" /></label>
                                                <form:select class="form-control"  id="contractTypeSel"  path="contractType">
                                                    <option <c:if test="${'serv'.equals(employee.contractType)}">selected="true"</c:if> value="serv"><spring:message code="people.employee.contractType.serv" /></option>
                                                    <option <c:if test="${'ind'.equals(employee.contractType)}">selected="true"</c:if> value="ind"><spring:message code="people.employee.contractType.ind" /></option>
                                                </form:select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.salary.label" /></label>
                                                <form:input class="form-control"  id="salaryTxt" path="salary"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.anualBonus.label" /></label>
                                                <form:input class="form-control"  id="anualBonusTxt" path="anualBonus"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.admissionDate.label" /></label>
                                                <form:input class="form-control"  id="admissionDateTxt" path="admissionDate"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.initDate.label" /></label>
                                                <form:input class="form-control"  id="initDateTxt" path="initDate"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.retirementDate.label" /></label>
                                                <form:input class="form-control"  id="retirementDateTxt" path="retirementDate"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.rh.label" /></label>
                                                <form:select class="form-control"  id="rhSel"  path="rh">
                                                    <option <c:if test="${'Apos'.equals(employee.rh)}">selected="true"</c:if> value="Apos"><spring:message code="people.employee.rh.Apos" /></option>
                                                    <option <c:if test="${'Aneg'.equals(employee.rh)}">selected="true"</c:if> value="Aneg"><spring:message code="people.employee.rh.Aneg" /></option>
                                                    <option <c:if test="${'Bpos'.equals(employee.rh)}">selected="true"</c:if> value="Bpos"><spring:message code="people.employee.rh.Bpos" /></option>
                                                    <option <c:if test="${'Bneg'.equals(employee.rh)}">selected="true"</c:if> value="Bneg"><spring:message code="people.employee.rh.Bneg" /></option>
                                                    <option <c:if test="${'ABpos'.equals(employee.rh)}">selected="true"</c:if> value="ABpos"><spring:message code="people.employee.rh.ABpos" /></option>
                                                    <option <c:if test="${'Opos'.equals(employee.rh)}">selected="true"</c:if> value="Opos"><spring:message code="people.employee.rh.Opos" /></option>
                                                    <option <c:if test="${'Oneg'.equals(employee.rh)}">selected="true"</c:if> value="Oneg"><spring:message code="people.employee.rh.Oneg" /></option>
                                                </form:select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.medCertificatesExpiration.label" /></label>
                                                <form:input class="form-control"  id="medCertificatesExpirationTxt" path="medCertificatesExpiration"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.licenseNumber.label" /></label>
                                                <form:input class="form-control"  id="licenseNumberTxt" path="licenseNumber"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.passportNumber.label" /></label>

                                                <form:input class="form-control"  id="passportNumberTxt" path="passportNumber"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.passportExpiration.label" /></label>
                                                <form:input class="form-control"  id="passportExpirationTxt" path="passportExpiration"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.americanVisaNumber.label" /></label>
                                                <form:input class="form-control"  id="americanVisaNumberTxt" path="americanVisaNumber"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.americanVisaExpiration.label" /></label>
                                                <form:input class="form-control"  id="americanVisaExpirationTxt" path="americanVisaExpiration"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.adminStatus.label" /></label>
                                                <form:select class="form-control"  id="adminStatusSel"  path="adminStatus">
                                                    <option <c:if test="${'active'.equals(employee.adminStatus)}">selected="true"</c:if> value="active"><spring:message code="people.employee.adminStatus.active" /></option>
                                                    <option <c:if test="${'inactive'.equals(employee.adminStatus)}">selected="true"</c:if> value="inactive"><spring:message code="people.employee.adminStatus.inactive" /></option>
                                                    <option <c:if test="${'suspended'.equals(employee.adminStatus)}">selected="true"</c:if> value="suspended"><spring:message code="people.employee.adminStatus.suspended" /></option>
                                                    <option <c:if test="${'waiting'.equals(employee.adminStatus)}">selected="true"</c:if> value="waiting"><spring:message code="people.employee.adminStatus.waiting" /></option>
                                                </form:select>    
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="people.employee.flyStatus.label" /></label>
                                                <form:select class="form-control"  id="flyStatusSel"  path="flyStatus">
                                                    <option <c:if test="${'active'.equals(employee.flyStatus)}">selected="true"</c:if> value="active"><spring:message code="people.employee.flyStatus.active" /></option>
                                                    <option <c:if test="${'inactive'.equals(employee.flyStatus)}">selected="true"</c:if> value="inactive"><spring:message code="people.employee.flyStatus.inactive" /></option>
                                                    <option <c:if test="${'onlyNational'.equals(employee.flyStatus)}">selected="true"</c:if> value="onlyNational"><spring:message code="people.employee.flyStatus.onlyNational" /></option>
                                                </form:select>    
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="employee-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                    <a href="employee-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>

        </div>   
        <%@ include file="/WEB-INF/views/people/employee/editJsFunctions.jsp" %>    
    </body>
</html>    
