<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.commercial.customer' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="customer-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.create' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> <spring:message code="commercial.customer.create.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.create" /></p>
                            </div>

                            <form:form class="form" method="post" action="customer-save.htm" modelAttribute="customer" id="createFrm">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.person.label" /></label>
                                                <form:select class="form-control"  id="personSel" items="${personList}" path="person.id" itemLabel="completeName" itemValue="id"/>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.company.label" /></label>

                                                <form:select class="form-control"  id="companySel" items="${companyList}" path="company.id" itemLabel="nameAddress" itemValue="id"/>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.accountExecutive.label" /></label>
                                                <form:select class="form-control"  id="accountExecutiveSel" items="${accountExecutiveList}" path="accountExecutive.id" itemLabel="person.completeName" itemValue="id"/>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.businessUnit.label" /></label>
                                                <form:select class="form-control"  id="businessUnitSel" items="${bussinessUnitList}" path="businessUnit.id" itemLabel="name" itemValue="id"/>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.year.label" /></label>
                                                <form:input class="form-control"  id="yearTxt" path="year"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.status.label" /></label>
                                                <form:select  class="form-control"   id="statusSel"  path="status">
                                                    <option value="active"><spring:message code="commercial.customer.status.active" /></option>
                                                    <option value="inactive"><spring:message code="commercial.customer.status.inactive" /></option>
                                                </form:select>  
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"><spring:message code="commercial.customer.details.label" /></label>
                                                <form:input class="form-control"  id="detailsTxt" path="details"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="customer-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                    <a href="customer-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>

        </div>   
        <%@ include file="/WEB-INF/views/commercial/customer/editJsFunctions.jsp" %>    
    </body>
</html>    
