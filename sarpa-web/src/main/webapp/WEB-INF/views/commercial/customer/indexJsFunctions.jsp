<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },pageSettings: { pageSize: 50 },
            columns: [
                    { field: "clientType", headerText: '<spring:message code="commercial.customer.clientType.label" />', width: 105 },
                    { field: "name", headerText: '<spring:message code="commercial.customer.name.label" />', width: 105 },
                    { field: "accountExecutive", headerText: '<spring:message code="commercial.customer.accountExecutive.label" />', width: 105 },
                    { field: "businessUnit", headerText: '<spring:message code="commercial.customer.businessUnit.label" />', width: 105 },
                    { field: "year", headerText: '<spring:message code="commercial.customer.year.label" />', width: 105 },
                    { field: "status", headerText: '<spring:message code="commercial.customer.status.label" />', width: 105 },
                    { field: "details", headerText: '<spring:message code="commercial.customer.details.label" />', width: 105 },
                    { field: "code", headerText: '<spring:message code="commercial.customer.code.label" />', width: 105 },
                    { field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right }
            ]
        });
        
    });
</script>