<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                name : {
                        required: true
                    },
                description : {
                        required: true
                    },
                },
            messages: {
                name : {
                        required: '<spring:message code="commercial.airAmbulance.name.errors.required" />'
                },
                description : {
                        required: '<spring:message code="commercial.airAmbulance.description.errors.required" />'
                },
			}
        });
    });
</script>