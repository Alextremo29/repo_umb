<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                {field: "code", headerText: "<spring:message code="commercial.customerRequest.code.label" />", width: 105},
                {field: "caller", headerText: '<spring:message code="commercial.customerRequest.caller.label" />', width: 105},
                {field: "probableflightdate", headerText: '<spring:message code="commercial.customerRequest.probableflightdate.label" />', width: 105},
                {field: "probableflighthour", headerText: '<spring:message code="commercial.customerRequest.probableflighthour.label" />', width: 105},
                {field: "company", headerText: '<spring:message code="commercial.customerRequest.company.label" />', width: 105},
                {field: "businessUnit", headerText: '<spring:message code="commercial.customerRequest.businessunit.label" />', width: 105},
                {field: "serviceType", headerText: '<spring:message code="commercial.customerRequest.typeofservice.label" />', width: 105},
                {field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right}
            ]
        });
        
    });
</script>