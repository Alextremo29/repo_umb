<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <script src="/resources/customRequestForm.js"></script>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.parametric.customerRequest' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="customerRequest-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.create' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> 
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso1 eq true}">
                                <spring:message code="commercial.customerRequest.create.case1.title" />
                            </c:if>
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso2 eq true}">
                            </c:if>
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso3 eq true}">
                                <spring:message code="commercial.customerRequest.create.case3.title" />
                            </c:if>
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso4 eq true}">
                                <spring:message code="commercial.customerRequest.create.case4.title" />
                            </c:if>
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso5 eq true}">
                                <spring:message code="commercial.customerRequest.create.case5.title" />
                            </c:if>
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso6 eq true}">
                                <spring:message code="commercial.customerRequest.create.case6.title" />
                            </c:if>
                            <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso7 eq true}">
                                <spring:message code="commercial.customerRequest.create.case7.title" />
                            </c:if>
                        </h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.create" /></p>
                            </div>

                            <form:form class="form" method="post" action="customerRequest-update.htm" modelAttribute="customerRequest" id="createFrm">
                                <form:hidden id="idHdn" path="id"/>
                                <form:hidden id="codeHdn" path="code"/>
                                <form:hidden id="businessUnitHdn" path="businessUnit.id"/>
                                <form:hidden id="callerHdn" path="caller"/>
                                <form:hidden id="companyHdn" path="company.id"/>
                                <form:hidden id="companyNameHdn" path="company.name"/>
                                <form:hidden id="serviceTypeHdn" path="serviceType.id"/>
                                <form:hidden id="probableflightdateHdn" path="probableflightdate"/>
                                <form:hidden id="probableflighthourHdn" path="probableflighthour"/>
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                        <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso1 eq true}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idConfiguration"  ><spring:message code="commercial.customerRequest.configuration.label" /></label>
                                                        <form:select class="form-control"  id="idConfiguration" items="${airAmbulanceList}" path="airAmbulance.id" itemLabel="name" itemValue="id"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idRequiresAmbulance"  ><spring:message code="commercial.customerRequest.requiresAmbulance.label" /></label>
                                                        <form:select  class="form-control"   id="idRequiresAmbulance"  path="requiresGroundAmbulance">
                                                            <option value="true"><spring:message code="commercial.customerRequest.requiresAmbulance.true" /></option>
                                                            <option value="false"><spring:message code="commercial.customerRequest.requiresAmbulance.false" /></option>
                                                        </form:select>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="detailTxt"  ><spring:message code="commercial.customerRequest.servicetype.details.label" /></label>
                                                        <form:input type="text" id="detailTxt" path="detail" class="form-control" placeholder="Detalle" />
                                                    </div>     
                                                </div>    
                                            </div>
                                        </c:if>
                                        <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso2 eq true}"> 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idreqpatcon"  ><spring:message code="commercial.customerRequest.servicetype.includesRescueEquipment.label" /></label>
                                                            <form:select  class="form-control"   id="idIncludesRescueEquipment"  path="includesRescueEquipment">
                                                                <option value="true"><spring:message code="commercial.customerRequest.servicetype.includesRescueEquipment.true" /></option>
                                                                <option value="false"><spring:message code="commercial.customerRequest.servicetype.includesRescueEquipment.false" /></option>
                                                            </form:select>  
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="numberOfPassengersTxt"  ><spring:message code="commercial.customerRequest.servicetype.numberOfPassengers.label" /></label>
                                                            <form:input type="text" id="numberOfPassengersTxt" path="numberOfPassengers" class="form-control" placeholder="N�mero de Pasajeros" />
                                                        </div>
                                                    </div>  
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="typeorganTxt"  ><spring:message code="commercial.customerRequest.servicetype.typeoforgan.label" /></label>
                                                            <form:input type="text" id="typeorganTxt" path="typeoforgan" class="form-control" placeholder="Tipo de �rgano" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idreqpatcon"  ><spring:message code="commercial.customerRequest.servicetype.requiresGroundAmbulance.label" /></label>
                                                            <form:select  class="form-control"   id="idRequiresGroundAmbulance"  path="requiresGroundAmbulance">
                                                                <option value="true"><spring:message code="commercial.customerRequest.servicetype.requiresGroundAmbulance.true" /></option>
                                                                <option value="false"><spring:message code="commercial.customerRequest.servicetype.requiresGroundAmbulance.false" /></option>
                                                            </form:select>  
                                                        </div>     
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="detailTxt"  ><spring:message code="commercial.customerRequest.servicetype.details.label" /></label>
                                                            <form:input type="text" id="detailTxt" path="detail" class="form-control" placeholder="Detalle" />
                                                        </div>     
                                                    </div>
                                                </div>            
                                        </c:if>     
                                        <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso3 eq true}"> 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idRequiresOxygen"  ><spring:message code="commercial.customerRequest.requiresOxygen.label" /></label>
                                                            <form:select  class="form-control"   id="idRequiresOxygen"  path="requiresOxygen">
                                                                <option value="false"><spring:message code="commercial.customerRequest.requiresOxygen.false" /></option>
                                                                <option value="true"><spring:message code="commercial.customerRequest.requiresOxygen.true" /></option>
                                                            </form:select>  
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idRequiresAmbulance"  ><spring:message code="commercial.customerRequest.requiresAmbulance.label" /></label>
                                                            <form:select  class="form-control"   id="idRequiresAmbulance"  path="requiresGroundAmbulance">
                                                                <option value="true"><spring:message code="commercial.customerRequest.requiresAmbulance.true" /></option>
                                                                <option value="false"><spring:message code="commercial.customerRequest.requiresAmbulance.false" /></option>
                                                            </form:select>  
                                                        </div>
                                                    </div>  
                                                </div>
                                                <div class="row" id = "divFiO2">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idOxygenFiO2"  ><spring:message code="commercial.customerRequest.oxygenFiO2.label" /></label>
                                                            <form:input type="text" id="idOxygenFiO2" path="oxygenFiO2" class="form-control" placeholder="FiO2" /> 
                                                        </div>
                                                    </div>        
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idRequiresTicket"  ><spring:message code="commercial.customerRequest.requiresTicket.label" /></label>
                                                            <form:select  class="form-control"   id="idRequiresTicket"  path="requiresTicket">
                                                                <option value="true"><spring:message code="commercial.customerRequest.requiresTicket.true" /></option>
                                                                <option value="false"><spring:message code="commercial.customerRequest.requiresTicket.false" /></option>
                                                            </form:select>  
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idTravelWithCompanion"  ><spring:message code="commercial.customerRequest.travelWithCompanion.label" /></label>
                                                            <form:select  class="form-control"   id="idTravelWithCompanion"  path="travelWithCompanion">
                                                                <option value="true"><spring:message code="commercial.customerRequest.travelWithCompanion.true" /></option>
                                                                <option value="false"><spring:message code="commercial.customerRequest.travelWithCompanion.false" /></option>
                                                            </form:select>  
                                                        </div>
                                                    </div>        
                                                </div>    
                                                <div class="row">
                                                   <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idAuthorizedTreatingDoctor"  ><spring:message code="commercial.customerRequest.authorizedTreatingDoctor.label" /></label>
                                                            <form:select  class="form-control"   id="idAuthorizedTreatingDoctor"  path="authorizedTreatingDoctor">
                                                                <option value="true"><spring:message code="commercial.customerRequest.authorizedTreatingDoctor.true" /></option>
                                                                <option value="false"><spring:message code="commercial.customerRequest.authorizedTreatingDoctor.false" /></option>
                                                            </form:select>  
                                                        </div>
                                                    </div> 
                                                </div>         
                                        </c:if>
                                        <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso4 eq true}">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idCityOfService"  ><spring:message code="commercial.customerRequest.cityOfService.label" /></label>
                                                            <form:input type="text" id="idCityOfService" path="cityOfService" class="form-control" placeholder="Ciudad de Servicio" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idBirthPlace"  ><spring:message code="commercial.customerRequest.birthplace.label" /></label>
                                                            <form:input type="text" id="idBirthPlace" path="birthPlace" class="form-control" placeholder="Lugar de Origen" />  
                                                        </div>     
                                                    </div>
                                            </div> 
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idDestinationPlace"  ><spring:message code="commercial.customerRequest.destinationPlace.label" /></label>
                                                            <form:input type="text" id="idDestinationPlace" path="destinationPlace" class="form-control" placeholder="Lugar Destino" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idDetails3"  ><spring:message code="commercial.customerRequest.details33.label" /></label>
                                                            <form:input type="text" id="idDetails3" path="detail" class="form-control" placeholder="Detalles" />  
                                                        </div>     
                                                    </div>
                                            </div> 
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idConfiguration"  ><spring:message code="commercial.customerRequest.configuration.label" /></label>
                                                            <form:select class="form-control"  id="idConfiguration" items="${configurationGAList}" path="configurationGA.id" itemLabel="name" itemValue="id"/>   
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="idType"  ><spring:message code="commercial.customerRequest.type.label" /></label>
                                                            <form:select  class="form-control"   id="idType"  path="type">
                                                                <option value="Intramunicipal"><spring:message code="commercial.customerRequest.type.intramunicipal.label" /></option>
                                                                <option value="Municipal"><spring:message code="commercial.customerRequest.type.intermunicipal.label" /></option>
                                                            </form:select>   
                                                        </div>     
                                                    </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso5 eq true}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idNumberOfPassengers"  ><spring:message code="commercial.customerRequest.numberOfPassengers.label" /></label>
                                                        <form:input type="text" id="idNumberOfPassengers" path="numberOfPassengers" class="form-control" placeholder="N�mero de pasajeros" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idTypeOfPassenger"  ><spring:message code="commercial.customerRequest.typeOfPassenger.label" /></label>
                                                        <form:input type="text" id="idTypeOfPassenger" path="typeOfPassenger" class="form-control" placeholder="Tipo de pasajero" />
                                                    </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idMedicalCrewOnBoard"  ><spring:message code="commercial.customerRequest.medicalCrewOnBoard.label" /></label>
                                                        <form:select  class="form-control"   id="idMedicalCrewOnBoard"  path="medicalCrewOnBoard">
                                                            <option value="true"><spring:message code="commercial.customerRequest.medicalCrewOnBoard.true" /></option>
                                                            <option value="false"><spring:message code="commercial.customerRequest.medicalCrewOnBoard.false" /></option>
                                                        </form:select>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idRequiredGroundAmbulance"  ><spring:message code="commercial.customerRequest.requiredGroundAmbulance.label" /></label>
                                                        <form:select  class="form-control"   id="idRequiredGroundAmbulance"  path="requiredGroundAmbulance">
                                                            <option value="true"><spring:message code="commercial.customerRequest.requiredGroundAmbulance.true" /></option>
                                                            <option value="false"><spring:message code="commercial.customerRequest.requiredGroundAmbulance.false" /></option>
                                                        </form:select>  
                                                    </div>  
                                                </div>        
                                             </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idGroundAmbulanceConfiguration"  ><spring:message code="commercial.customerRequest.groundAmbulanceConfiguration.label" /></label>
                                                        <form:select class="form-control"  id="idGroundAmbulanceConfiguration" items="${configurationGAList}" path="configurationGA.id" itemLabel="name" itemValue="id"/>   
                                                    </div>  
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="idRequiredCaterinService"  ><spring:message code="commercial.customerRequest.requiredCaterinService.label" /></label>
                                                        <form:select  class="form-control"   id="idRequiredCaterinService"  path="requiredCaterinService">
                                                            <option value="true"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
                                                            <option value="false"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
                                                        </form:select>  
                                                    </div>
                                                </div>       
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idDetails36"  ><spring:message code="commercial.customerRequest.details36.label" /></label>
                                                        <form:input type="text" id="idDetails36" path="detail" class="form-control" placeholder="Detalles" />
                                                    </div>
                                                </div> 
                                            </div>
                                     </c:if>
                                    <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso6 eq true}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idNumberOfPassengers"  ><spring:message code="commercial.customerRequest.numberOfPassengers.label" /></label>
                                                        <form:input type="text" id="idNumberOfPassengers" path="numberOfPassengers" class="form-control" placeholder="N�mero de pasajeros" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idTypeOfPassenger"  ><spring:message code="commercial.customerRequest.typeOfPassenger.label" /></label>
                                                        <form:input type="text" id="idTypeOfPassenger" path="typeOfPassenger" class="form-control" placeholder="Tipo de pasajero" />
                                                    </div>
                                                </div>
                                             </div>
                                             <div class="row">        
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="idRequiredCaterinService"  ><spring:message code="commercial.customerRequest.requiredCaterinService.label" /></label>
                                                        <form:select  class="form-control"   id="idRequiredCaterinService"  path="requiredCaterinService">
                                                            <option value="true"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
                                                            <option value="false"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
                                                        </form:select>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="idDetails38"  ><spring:message code="commercial.customerRequest.details36.label" /></label>
                                                        <form:input type="text" id="idDetails38" path="detail" class="form-control" placeholder="Detalles" />
                                                    </div>
                                                </div>        
                                            </div>
                                    </c:if>
                                    <c:if test="${customerRequest.existeDetalleServicioEnSolicitudCaso7 eq true}">
                                        <div class="row">        
                                            <div class="col-md-6">    
                                                <div class="form-group">
                                                    <label for="idLoadType"  ><spring:message code="commercial.customerRequest.loadType.label" /></label>
                                                    <form:select class="form-control"  id="idLoadType" items="${loadTypeList}" path="loadType.id" itemLabel="name" itemValue="id"/>
                                                </div>  
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id ="fieldOtherLoadType">
                                                    <label for="idOtherLoadType"  ><spring:message code="commercial.customerRequest.otherLoadType.label" /></label>
                                                    <form:input type="text" id="idOtherLoadType" path="otherLoadType" class="form-control" placeholder="Otro tipo de carga" />
                                                </div>
                                            </div>        
                                        </div>
                                        <div class="row">        
                                            <div class="col-md-6">    
                                                  <div class="form-group">
                                                    <label for="idLoadTypeLongNA"  ><spring:message code="commercial.customerRequest.loadType.long.required.label" /></label> 
													<form:select  class="form-control"  id="idLoadTypeLongNA"  path="loadTypeLongNA">
														<option value="Si"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
														<option value="No"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
													</form:select>   		
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="idLoadTypeLong"  ><spring:message code="commercial.customerRequest.loadType.long.label" /></label>
                                                    <form:input type="text" id="idloadTypeLong" path="loadTypeLong" class="form-control" placeholder="Largo en cms" />
                                                </div>
                                            </div>        
                                        </div>
                                        <div class="row">        
                                            <div class="col-md-6">    
                                                <div class="form-group">
                                                    <label for="idLoadTypeWidthNA"  ><spring:message code="commercial.customerRequest.loadType.width.required.label" /></label>
													<form:select  class="form-control"  id="idLoadTypeWidthNA"  path="loadTypeWidthNA">
														<option value="Si"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
														<option value="No"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
													</form:select>  
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="idLoadTypeWidth"  ><spring:message code="commercial.customerRequest.loadType.width.label" /></label>
                                                    <form:input type="text" id="idLoadTypeWidth" path="loadTypeWidth" title="Unidad de medida cms" class="form-control" placeholder="Ancho en cms" />
                                                </div>
                                            </div>        
                                        </div>
                                        <div class="row">        
                                            <div class="col-md-6">    
                                                <div class="form-group">
                                                    <label for="idLoadTypeHighNA"  ><spring:message code="commercial.customerRequest.loadType.high.required.label" /></label>
													<form:select  class="form-control"  id="idLoadTypeHighNA"  path="loadTypeHighNA">
														<option value="Si"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
														<option value="No"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
													</form:select> 
                                                </div> 
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="idLoadTypeHigh"  ><spring:message code="commercial.customerRequest.loadType.high.label" /></label>
                                                    <form:input type="text" id="idloadTypeHigh" path="loadTypeHigh" class="form-control" placeholder="Alto en cms" />
                                                </div>
                                            </div>        
                                        </div>
                                        <div class="row">        
                                            <div class="col-md-6">    
                                                 
                                                <div class="form-group">
                                                    <label for="idLoadTypeWeightPerPieceNA"  ><spring:message code="commercial.customerRequest.loadType.weightPerPiece.required.label" /></label>
													<form:select  class="form-control"  id="idLoadTypeWeightPerPieceNA"  path="loadTypeWeightPerPieceNA">
														<option value="Si"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
														<option value="No"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
													</form:select> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="idLoadTypeWeightPerPiece"  ><spring:message code="commercial.customerRequest.loadType.weightPerPiece.label" /></label>
                                                    <form:input type = "text" id="idLoadTypeWeightPerPiece" path="loadTypeWeightPerPiece" class="form-control" placeholder="Peso en Kgs" />
                                                </div>
                                            </div>        
                                        </div>
                                        <div class="row">        
                                            <div class="col-md-6">    
                                                <div class="form-group">
                                                    <label for="idShipperDeclaration"  ><spring:message code="commercial.customerRequest.loadType.shipperDeclaration.label" /></label>
													<form:select  class="form-control"  id="idShipperDeclaration"  path="shipperDeclaration">
														<option value="Si"><spring:message code="commercial.customerRequest.requiredCaterinService.true" /></option>
														<option value="No"><spring:message code="commercial.customerRequest.requiredCaterinService.false" /></option>
													</form:select> 
                                                </div>  
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="idDetails39"  ><spring:message code="commercial.customerRequest.details36.label" /></label>
                                                    <form:input type="text" id="idDetails39" path="detail" class="form-control" placeholder="Detalles" />
                                                </div>
                                            </div>        
                                        </div>
                                    </c:if>
                                </div>
                                
                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="customerRequest-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                    <a id="createLnk" href="customerRequest-create.htm" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <spring:message code="commercial.customerRequest.create.back" />
                                    </a>
                                    <a href="customerRequest-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                    <a href="customerRequest-create3.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.saveandcontinue" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>

        </div>   
        <%@ include file="/WEB-INF/views/commercial/customerRequest/editJsFunctions.jsp" %>    
    </body>
</html>    
