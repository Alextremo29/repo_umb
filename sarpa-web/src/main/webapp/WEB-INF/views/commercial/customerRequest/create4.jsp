<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>
        <link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome/css/font-awesome.min.css">

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <%@ include file="/WEB-INF/views/common/tableJavaScripts.jsp" %>
        <script type="text/javascript">

            $(function () {
            });

        </script>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.parametric.customerRequest.wizard4' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="customerRequest-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.create' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> <spring:message code="menu.parametric.customerRequest.wizard4" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.create" /></p>
                            </div>

                            <form:form class="form" method="post" action="customerRequest-update.htm" modelAttribute="customerRequest" id="createFrm">
                                <form:hidden id="idHdn" path="id"/>
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idRequiredCaterin"  ><spring:message code="commercial.customerRequest.caterin.label" /></label>
                                                <form:select  class="form-control"   id="idRequiredCaterinService"  path="requiredCaterinService">
                                                    <option value="true"><spring:message code="common.form.true" /></option>
                                                    <option value="false"><spring:message code="common.form.false" /></option>
                                                </form:select>   
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueCaterin"  ><spring:message code="commercial.customerRequest.valuecaterin.label" /></label>
                                                <form:input type="text" id="idValueCaterin" path="valueCaterin" class="form-control" placeholder="Valor caterin" />
                                                <a title="Consultar" href="serviceType-index.htm" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=400'); return false;"><i class="fa fa-search-plus"></i></a>
                                            </div>
                                        </div>        
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idRequiredAirportRates"  ><spring:message code="commercial.customerRequest.airportrates.label" /></label>
                                                <form:select  class="form-control"   id="idRequiredAirportRates"  path="requiredAirportRates">
                                                    <option value="true"><spring:message code="common.form.true" /></option>
                                                    <option value="false"><spring:message code="common.form.false" /></option>
                                                </form:select>   
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueAirportRates"  ><spring:message code="commercial.customerRequest.valueairportrates.label" /></label>
                                                <form:input type="text" id="idValueAirportRates" path="valueAirportRates" class="form-control" placeholder="Valor Tasas Aeroportuarias" />
                                                <a id="idOpenAirportRates" title="Consultar" href="airportrate-index.htm" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=400'); return false;" ><i class="fa fa-search-plus"></i></a>
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idRequiredAirportExtensionHour"  ><spring:message code="commercial.customerRequest.airportextensionhour.label" /></label>
                                                <form:select  class="form-control"   id="idRequiredAirportExtensionHour"  path="requiredAirportExtension">
                                                    <option value="true"><spring:message code="common.form.true" /></option>
                                                    <option value="false"><spring:message code="common.form.false" /></option>
                                                </form:select>   
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueAirportExtensionHour"  ><spring:message code="commercial.customerRequest.valueairportextensionhour.label" /></label>
                                                <form:input type="text" id="idValueAirportExtensionHour" path="valueAirportExtension" class="form-control" placeholder="Valor Tasas Aeroportuarias" />
                                                <a href="airportextensionhour-index.htm" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=400'); return false;" title="Consultar"><i class="fa fa-search-plus"></i></a>
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idFBO"  ><spring:message code="commercial.customerRequest.fbo.label" /></label>
                                                <form:select  class="form-control"   id="idFBO"  path="requiredFBO">
                                                    <option value="true"><spring:message code="common.form.true" /></option>
                                                    <option value="false"><spring:message code="common.form.false" /></option>
                                                </form:select>   
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueFBO"  ><spring:message code="commercial.customerRequest.valueairportextensionhour.label" /></label>
                                                <form:input type="text" id="idValueFBO" path="valueFBO" class="form-control" placeholder="Valor FBO" />
                                                <a href="fbocost-index.htm" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=400'); return false;" title="Consultar"><i class="fa fa-search-plus"></i></a>
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idRequiresGroundAmbulance" ><spring:message code="commercial.customerRequest.groundambulance.label" /></label>
                                                <form:select  class="form-control"   id="idRequiresGroundAmbulance"  path="requiresGroundAmbulance">
                                                    <option value="true"><spring:message code="common.form.true" /></option>
                                                    <option value="false"><spring:message code="common.form.false" /></option>
                                                </form:select>   
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueGroundAmbulance"  ><spring:message code="commercial.customerRequest.valuegroundambulance.label" /></label>
                                                <form:input type="text" id="idValueGroundAmbulance" path="valueGroundAmbulance" class="form-control" placeholder="Valor Ambulancia Terrestre" />
                                                <a href="groundAmbulance-index.htm" target="popup" onClick="window.open(this.href, this.target, 'width=500,height=400'); return false;" title="Consultar"><i class="fa fa-search-plus"></i></a>
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idFlighttimeQuote" ><spring:message code="commercial.customerRequest.flighttimetoquote.label" /></label>
                                                <form:input type="text" id="idValueGroundAmbulance" path="valueGroundAmbulance" class="form-control" placeholder="Tiempo de vuelo a cotizar" readonly="true"/>            
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueFlighttimeQuote"  ><spring:message code="commercial.customerRequest.valueflighttimetoquote.label" /></label>
                                                <form:input type="text" id="idValueFlighttimeQuote" path="valueFlighttimeQuote" class="form-control" placeholder="Valor Tiempo a Cotizar" />
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idSpecialConfiguration" ><spring:message code="commercial.customerRequest.specialconfiguration.label" /></label>
                                                <form:select  class="form-control"   id="idSpecialConfiguration"  path="requiredSpecialConfiguration">
                                                    <option value="true"><spring:message code="common.form.true" /></option>
                                                    <option value="false"><spring:message code="common.form.false" /></option>
                                                </form:select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idSpecialConfiguration"  ><spring:message code="commercial.customerRequest.valuespecialconfiguration.label" /></label>
                                                <form:input type="text" id="idValueSpecialConfiguration" path="valueSpecialConfiguration" class="form-control" placeholder="Valor Configuración Especial" />
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idOtherServices" ><spring:message code="commercial.customerRequest.othersservices.label" /></label>
                                                <form:input type="text" id="idOtherServices" path="otherServices" class="form-control" title="Otros Servicios" />            
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idValueOtherServices"  ><spring:message code="commercial.customerRequest.valueothersservices.label" /></label>
                                                <form:input type="text" id="idValueOtherServices" path="valueOtherServices" class="form-control" title="Valor Otros servicios" />
                                            </div>
                                        </div>        
                                    </div>  
                                </div>                

                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="customerRequest-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                    <a id="createLnk" href="customerRequest-create3.htm" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <spring:message code="commercial.customerRequest.create.back" />
                                    </a>
                                    <a href="customerRequest-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                    <a href="customerRequest-create5.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.saveandcontinue" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>
            <%@ include file="/WEB-INF/views/commercial/customerRequest/editJsFunctions.jsp" %>    
    </body>
</html>    
