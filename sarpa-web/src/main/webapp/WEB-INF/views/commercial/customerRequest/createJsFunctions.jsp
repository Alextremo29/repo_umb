<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                businessUnit : {
                    required: true
                },
                caller : {
                    required: true
                },
                company : {
                    required: true
                },
                serviceType : {
                    required: true
                },
                probableflightdate : {
                    required: true
                },
                probableflighthour : {
                    required: true,
                    regex: /^(?:[01]\d|2[0-3]):?[0-5]\d$/
                }
            },
            messages: {
                businessUnit : {
                        required: '<spring:message code="commercial.customerRequest.businessunit.required.label" />'
                },
                caller : {
                        required: '<spring:message code="commercial.customerRequest.caller.required.label" />'
                },
                company : {
                        required: '<spring:message code="commercial.customerRequest.company.required.label" />'
                },
                serviceType : {
                        required: '<spring:message code="commercial.customerRequest.typeofservice.required.label" />'
                },
                probableflightdate : {
                        required: '<spring:message code="commercial.customerRequest.probableflightdate.required.label" />'
                },
                probableflighthour : {
                        required: '<spring:message code="commercial.customerRequest.probableflighthour.required.label" />',
                        regex: '_REGEX'
                }
            }
        });
    });
</script>