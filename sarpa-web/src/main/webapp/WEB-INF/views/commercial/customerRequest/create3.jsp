<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <script src="resources/js/eventsCustomerRequest.js"></script>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.parametric.customerRequest' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="customerRequest-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.create' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> <spring:message code="commercial.customerRequest.create.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.create" /></p>
                            </div>

                            <form:form class="form" method="post" action="customerRequest-update.htm" modelAttribute="customerRequest" id="createFrm">
                                <form:hidden id="idHdn" path="id"/>
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="originTxt"  ><spring:message code="commercial.customerRequest.origin.label" /></label>
                                                <form:input type="text" id="originTxt" path="origin.name" class="form-control"/>
                                                <form:hidden id="originHdn" path="origin.id"/>
                                                <div id="origins"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="destinationTxt"  ><spring:message code="commercial.customerRequest.destination.label" /></label>
                                                <form:input type="text" id="destinationTxt" path="destination.name" class="form-control"/>
                                                <form:hidden id="destinationHdn" path="destination.id"/>
                                                <div id="destinations"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idRoute"  ><spring:message code="commercial.customerRequest.airplanetype.label" /></label>
                                                <form:select class="form-control"  id="airplaneTypeId" items="${airplaneTypeServiceList}" path="airplaneType.id" itemLabel="name" itemValue="id"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class ="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idRoute"  ><spring:message code="commercial.customerRequest.route.label" /></label>
                                                <a id="addJourney" title="Agregar Nuevo Trayecto">
                                                    <img border="0" alt="Agregar Nuevo Trayecto" src="assets/images/ico/add.png" width="25" height="25">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id ="journeys" class="row">

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="timetoquoteTxt"  ><spring:message code="commercial.customerRequest.timetoquote.label" /></label>
                                                <form:input type="text" id="timetoquoteTxt" path="timetoquote" class="form-control" />        
                                            </div>		
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="flighttimeTxt"  ><spring:message code="commercial.customerRequest.flighttime.label" /></label>
                                                <form:input type="text" id="flighttimeTxt" path="flightTime" class="form-control" readonly="yes"/>        
                                            </div>		
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idObservationsTxt"  ><spring:message code="commercial.customerRequest.observations.label" /></label>
                                                <form:input type="text" id="idObservationsTxt" path="observations" class="form-control" itemLabel="name" itemValue="nameCompany" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="customerRequest-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                    <a id="createLnk" href="customerRequest-create2.htm" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <spring:message code="commercial.customerRequest.create.back" />
                                    </a>
                                    <a href="customerRequest-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                    <a href="customerRequest-create4.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.saveandcontinue" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <%@ include file="/WEB-INF/views/commercial/customerRequest/editJsFunctions.jsp" %>    
    </body>
</html>    
