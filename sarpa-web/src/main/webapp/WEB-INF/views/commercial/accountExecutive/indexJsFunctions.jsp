<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                { field: "code", headerText: "<spring:message code="commercial.accountExecutive.code.label" />", width: 105 },
                { field: "person", headerText: "<spring:message code="commercial.accountExecutive.person.label" />", width: 105 },
                { field: "detail", headerText: "<spring:message code="commercial.accountExecutive.detail.label" />", width: 105 },
                { field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right }       
            ]
        });
        
    });
</script>