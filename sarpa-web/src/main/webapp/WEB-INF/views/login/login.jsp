<div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
    <div class="card border-grey border-lighten-3 m-0">
        <div class="card-header no-border">
            <div class="card-title text-xs-center">
                <div class="p-1"><img src="assets/images/logo/logoSarpa.png" alt="branding logo"></div>
            </div>
            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span><spring:message code="login.title" /></span></h6>
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <form:form method="post" action="/sarpa/login.htm" modelAttribute="user" id="formulario-login">
                    <fieldset class="form-group position-relative has-icon-left mb-0">
                            <input type="text" class="form-control form-control-lg input-lg" id="usernameTxt" placeholder="Nombre de Usuario" required  name="username" />
                            <div class="form-control-position">
                                <i class="ft-user"></i>
                            </div>
                        </fieldset>
                        <fieldset class="form-group position-relative has-icon-left">
                            <input type="password" class="form-control form-control-lg input-lg" id="passwordPass" placeholder="Contraseņa" required name="password"/>
                            <div class="form-control-position">
                                <i class="fa fa-key"></i>
                            </div>
                        </fieldset>

                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i><spring:message code="login.submit" /></button>
                    
                </form:form> 
            </div>
        </div>
    </div>
</div>  