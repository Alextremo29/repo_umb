<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="SARPA Admin">
        <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="SARPA">
        <spring:message code="index.title" />
        <link rel="apple-touch-icon" href="assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/fonts/feather/style.min.css">               <!-- Menu superior -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome/css/font-awesome.min.css"><!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flag-icon-css/css/flag-icon.min.css">  <!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/css/pace.css">            <!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/css/weather-icons/climacons.min.css"><!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/meteocons/style.css">                  <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="assets/css/app.css">                                <!-- Menu Lateral -->

        <link rel="stylesheet" type="text/css" href="assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="assets/css/core/menu/menu-types/vertical-overlay-menu.css">
        <link rel="stylesheet" type="text/css" href="assets/fonts/simple-line-icons/style.css">            <!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        
        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <!-- END Custom CSS-->
        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <%@ include file="/WEB-INF/views/common/tableJavaScripts.jsp" %>
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

        <!-- navbar-fixed-top-->
        <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-light navbar-border navbar-brand-center">
            <div class="navbar-wrapper">
                <div class="navbar-header">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item">
                            <a href="" class="navbar-brand">
                                <img alt="stack admin logo" src="assets/images/logo/logoSarpa.png"
                                     class="brand-logo"><br/>
                                
                            </a>
                        </li>
                        <li class="nav-item hidden-md-up float-xs-right">
                            <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-container content container-fluid">
                    <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                        <ul class="nav navbar-nav">
                            <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu"></i></a></li>

                            <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon ft-maximize"></i></a></li>
                            <li class="nav-item nav-search"><a href="#" class="nav-link nav-link-search"><i class="ficon ft-search"></i></a>
                                <div class="search-input">
                                    <input type="text" placeholder="Buscar" class="input">
                                </div>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav float-xs-right">

                            <li class="dropdown dropdown-notification nav-item">
                                <a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon ft-bell"></i>
                                    <span class="tag tag-pill tag-default tag-danger tag-default tag-up">5</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0">
                                            <span class="grey darken-2">Notificaciones</span>
                                            <span class="notification-tag tag tag-default tag-danger float-xs-right m-0">5 Nuevas</span>
                                        </h6>
                                    </li>
                                    <li class="list-group scrollable-container">
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left valign-middle"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Pendiente revision de ...!</h6>
                                                    <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <small>
                                                        <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Hace 30 minutos</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left valign-middle"><i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading red darken-1">Alerta de ...</h6>
                                                    <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                                                    <small>
                                                        <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Hace 5 horas</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left valign-middle"><i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading yellow darken-3">Advertencia</h6>
                                                    <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                                                    <small>
                                                        <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Hoy</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">Leer todas las notificaciones</a></li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-notification nav-item">
                                <a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon ft-mail"></i>
                                    <span class="tag tag-pill tag-default tag-warning tag-default tag-up">3</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0">
                                            <span class="grey darken-2">Mensajes</span>
                                            <span class="notification-tag tag tag-default tag-warning float-xs-right m-0">4 Nuevos</span>
                                        </h6>
                                    </li>
                                    <li class="list-group scrollable-container">
                                        <a href="javascript:void(0)" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left">
                                                    <span class="avatar avatar-sm avatar-online rounded-circle">
                                                        <img src="assets/images/portrait/small/avatar-s-1.png" alt="avatar"><i></i></span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Mensaje de: Andres...</h6>
                                                    <p class="notification-text font-small-3 text-muted">Adjunto..</p>
                                                    <small>
                                                        <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Hoy</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>



                                    </li>
                                    <li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">Leer todos los mensajes</a></li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-user nav-item">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                                    <span class="user-name">${userCompleteName}</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item"><i class="ft-user"></i> Editar Perfil</a>
                                    <a
                                        href="#" class="dropdown-item"><i class="ft-mail"></i> Mensajes</a>
                                    <a href="#" class="dropdown-item"><i class="ft-check-square"></i> Tareas</a>
                                    <a href="#" class="dropdown-item"><i class="ft-comment-square"></i> Conversaciones</a>
                                    <div class="dropdown-divider"></div>

                                    <form:form method="post" action="logout.htm"> 
                                        <button id="logOutBtn" type="submit" class="dropdown-item">
                                            <i class="ft-power"></i>
                                            <spring:message code="common.header.signOut" />
                                        </button>
                                    </form:form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <%@ include file="/WEB-INF/views/admin/menu.jsp" %>

        <div class="app-content content container-fluid">
            <div class="content-wrapper">
                <div class="content-header row">
                    <%@ include file="/WEB-INF/views/common/flashMessage.jsp" %>
                </div>
                <div class="content-body" id="divMainContent"><!-- Stats -->

                    <h1><spring:message code="dashboard.title" /></h1>
                    <h2><spring:message code="dashboard.intro" /></h2>
                </div>
            </div>
            
        </div>

        <script src="assets/vendors/js/vendors.min.js" type="text/javascript"></script> 
        <script src="assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="assets/js/core/app.js" type="text/javascript"></script>
        <script src="assets/js/sarpa/menu.js" type="text/javascript"></script>
        <%@ include file="/WEB-INF/views/common/commonJsFunctions.jsp" %>    
        <script src="resources/js/sessionUtilities.js"></script>
    </body>
</html>